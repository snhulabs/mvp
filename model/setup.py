from setuptools import setup, find_packages

setup(
    name='snhu_mvp',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'starlette==0.14.1',
        'uvicorn[standard]==0.12.3',
        'google-cloud-storage==1.38.0',
        'librosa==0.8.1',
        'opensmile==2.0.2',
        'numpy==1.20.3',
        'pandas==1.2.4',
        'praat-parselmouth==0.4.0',
        'passlib==1.7.4',
        'pydub==0.25.1',
        'scikit-learn==0.24.1',
    ],
    include_package_data=True
)
