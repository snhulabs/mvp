#!/bin/bash

set -euo pipefail;

export DEBIAN_FRONTEND=noninteractive;

apt-get update;
apt-get -y upgrade;

apt-get install -y libsndfile1 sox cmake ffmpeg build-essential;


