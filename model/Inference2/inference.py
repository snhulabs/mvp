"""Main Class for inference

Requirements:
	* A static meta data file should be present. It can be updated.
	* meta data (*.json) has audio file link and necessary open-smile outputs
		- csvFile1: Output of the execution with IS13_ComParE.conf.
				For this execution 3 parameters in FrameModeFunctionals.conf.inc
				should be updated as seen below:
				frameMode = fixed
				frameSize = 5
				frameStep = 3
		- csvFile2:Output of the execution with IS13_ComParE.conf with
				-lldcsvoutput argument.
	* .pkl file for the model

"""

import os
import random
import requests
import pickle
import json
import shutil
import librosa
import pandas as pd
import numpy as np
import parselmouth
from shutil import rmtree

from utils import timeit
from syllable_nuclei import speech_rate
from audio_chunk import get_chunks

FEATURES_NUM = 6375
OPENSMILE_TOP_BUFFER = 2
OPENSMILE_BOTTOM_BUFFER = 3
ROW_SKIP = FEATURES_NUM + OPENSMILE_TOP_BUFFER + OPENSMILE_BOTTOM_BUFFER

class InferenceProvider:

	def __init__(self, meta_path, step_size=3, window_size=5):
		"""Takes config details as param here.

		Configuration could  be read from the environment or a flat
		file (e.g. config.txt).

		Args:
			meta_path (str): json file path
			step_size (int): step size parsing audio
			window_size (int): window size parsing audio

		"""
		self.meta_path = meta_path
		self.step_size = step_size
		self.window_size = window_size
		self.token = None
		self.meta_data = None
		self.models = {}
		self.temp_folder_name = 'tmp'
		if not os.path.exists(self.temp_folder_name):
			os.mkdir(self.temp_folder_name)

	def _clean_up(self):
		shutil.rmtree(self.temp_folder_name)

	@timeit
	def request_jwt(self, base_url=None, user=None, pw=None):
		"""Requests and sets token."""
		user = user or os.getenv('MVP_USERNAME', '') # could be read from env vars
		pw = pw or os.getenv('MVP_PASSWORD', '') # could be read from env vars
		base_url = base_url or 'https://localhost:1337'
		r = requests.post(
			base_url + '/auth/local',
			data={'identifier':user, 'password':pw}
		)
		result  = r.json()
		self.token = result['jwt']

	@timeit
	def download_file(self, url, file_name):
		"""Downloads from `url` to save to `path`."""
		headers = {'Authorization':'Bearer ' + self.token}
		r = requests.get(url, headers=headers)
		full_path = os.path.join(self.temp_folder_name, file_name)
		with open(full_path, 'wb') as f:
			f.write(r.content)
		return full_path

	@timeit
	def read_audio(self, path):
		"""Reads audio data to process later."""
		duration = librosa.get_duration(filename=path)
		return int(duration)

	@timeit
	def read_meta(self, path):
		"""Reads metadata from a json file."""
		with open(path) as f:
			self.meta_data = json.load(f)

	@timeit
	def load_model(self, key, filename):
		self.models[key] = pickle.load(open(filename, 'rb'))

	@timeit
	def get_meta_by_id(self, session_id=None, attempt_id=None):
		"""Gets meta for given id from `self.meta_data`.

		Args:
			session_id (int):
			attempt_id (int):

		Returns:
			dict: requested object

		"""
		if session_id:
			for obj in self.meta_data:
				if obj['id'] == session_id:
					return obj

		if attempt_id:
			for obj in self.meta_data:
				for task, attempts in obj['tasks'].items():
					for attempt in attempts:
						if attempt['id'] == attempt_id:
							return attempt

	@timeit
	def run_by_attempt_id(self, id, always_read_meta=True):
		"""Sample runner to process attempt by id.

		Args:
			id (int): attempt id
			always_read_meta (bool): Whether to update `self.meta_data`,
				always. This is useful since flat json file might be updated
				frequently.

		Returns:
			pd.DataFrame: predictions by sample for each dimension

		"""
		if self.meta_data is None or always_read_meta:
			self.read_meta(self.meta_path)

		if self.token is None:
			self.request_jwt()

		obj = self.get_meta_by_id(attempt_id=id)

		temp_file_name = 'tmp_{}.wav'.format(random.randint(0, 10000))
		full_path = self.download_file(obj['audioFile'], temp_file_name)
		this_audio_length = self.read_audio(full_path)

		num_lines = sum(1 for line in open(obj['csvFile1']))
		keep = list(range(OPENSMILE_TOP_BUFFER, OPENSMILE_TOP_BUFFER + FEATURES_NUM + 1))
		to_exclude = list(range(OPENSMILE_TOP_BUFFER)) + list(range(OPENSMILE_TOP_BUFFER + FEATURES_NUM + 1, num_lines))
		features_names = pd.read_csv(
			obj['csvFile1'],
			skiprows=to_exclude,
			names=['features'],
			engine='python'
		).features.tolist()

		# these two csv will be read from remote server similar to the audio path read
		df1 = pd.read_csv(obj['csvFile1'], skiprows=ROW_SKIP, names=features_names)
		df2 = pd.read_csv(obj['csvFile2'])

		prediction = pd.DataFrame()
		for name, model in self.models.items():
			prediction[name] = model.predict_proba(df1)[:,1]
		return prediction

	@timeit
	def get_speech_rate(self, id):
		obj = self.get_meta_by_id(attempt_id=id)
		temp_file_name = 'tmp_{}.wav'.format(random.randint(0, 10000))
		full_path = self.download_file(obj['audioFile'], temp_file_name)
		chunk_names = get_chunks(full_path, self.window_size, self.step_size)
		# assumption: Avg number of syllable in a word is 1.2 for English
		# speech rate not calculated for chunks less than 5 seconds / window size
		return [
			speech_rate(f)["speechrate(nsyll / dur)"] * (60 / 1.2)
			for f in chunk_names
			if speech_rate(f)["dur(s)"] >= 5.0
		]

if __name__ == "__main__":
	inference = InferenceProvider(meta_path='input/sample_output.json')
	inference.load_model('friendliness', 'input/friendliness_model.pkl')
	inference.load_model('intelligence', 'input/intelligence_model.pkl')
	inference.load_model('likability', 'input/likability_model.pkl')
	inference.load_model('competence', 'input/competence_model.pkl')
	for id in [6201]:
		predictions = inference.run_by_attempt_id(id=id)
		print(predictions)

		rate = inference.get_speech_rate(id=id)
		print(rate)

	inference._clean_up()
