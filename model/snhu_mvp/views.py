import os
from tempfile import NamedTemporaryFile, TemporaryDirectory

from starlette.endpoints import HTTPEndpoint
from starlette.responses import JSONResponse

import opensmile
import pandas as pd

from snhu_mvp.inference import InferenceProvider

from snhu_mvp.utils import (
    download_file,
    upload_file,
    run_smile
)


class ProcessAudioView(HTTPEndpoint):

    async def post(self, request):
        data = await request.json()

        smile_data = {
            'csvs': {}
        }

        try:
            with TemporaryDirectory() as tmp_dir:
                audio_file = NamedTemporaryFile(dir=tmp_dir, suffix='.wav')
                audio_file_name = audio_file.name.replace('.wav', '')
                csv_is13_file = os.path.join(*[tmp_dir, f'{audio_file_name}_is13.csv'])
                csv_lld_file = os.path.join(*[tmp_dir, f'{audio_file_name}_lld.csv'])
                csv_predictions_file = os.path.join(*[tmp_dir, f'{audio_file_name}_predictions.csv'])
                csv_demo_file = os.path.join(*[tmp_dir, f'{audio_file_name}_demo.csv'])

                download_file(data['fileURI'], audio_file.name)

                # Get IS13 Compare results
                run_smile(audio_file.name, csv_is13_file, 'is09-13/IS13_ComParE.conf', csv_lld_file)
                inf = InferenceProvider()
                predictions = inf.run_predictions(csv_is13_file)
                smile_data['friendliness'] = predictions['friendliness']
                smile_data['intelligence'] = predictions['intelligence']
                smile_data['likability'] = predictions['likability']
                smile_data['competence'] = predictions['competence']

                predictions_df_data = []
                for i in range(len(predictions['friendliness'])):
                    pred_data = {}
                    for k, v in predictions.items():
                        pred_data[k] = predictions[k][i]
                    predictions_df_data.append(pred_data)

                predictions_df = pd.DataFrame(predictions_df_data)
                predictions_df.to_csv(csv_predictions_file)

                # Audio Speech Rate
                speech_rates = inf.get_speech_rate(audio_file.name)
                smile_data['syllable_rate'] = speech_rates

                # Get Volume/Intensity / Demo Config Results
                run_smile(audio_file.name, csv_demo_file, 'demo/demo1_energy.conf')

                demo_df = pd.read_csv(csv_demo_file, sep=';')
                demo_df['intensity'] = demo_df.apply(lambda x: x['pcm_LOGenergy'] + 20, axis=1)

                smile_data['csvs']['is13'] = upload_file(csv_is13_file)
                smile_data['csvs']['is13_lld'] = upload_file(csv_lld_file)
                smile_data['csvs']['predictions'] = upload_file(csv_predictions_file)
                smile_data['csvs']['demo_config'] = upload_file(csv_demo_file)

                audio_file.close()
        except Exception as e:
            raise Exception('Error with file `{}`: {}'.format(data['fileURI'], str(e)))

        return JSONResponse({
            'smileData': smile_data
        })
