import os

APP_NAME = 'snhu_mvp'

def get(name, default=None):
	return os.environ.get(name, default)

