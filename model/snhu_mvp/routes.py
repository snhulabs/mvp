from starlette.routing import Route, Mount

from snhu_mvp.views import ProcessAudioView


routes = [
	Route('/process_audio', endpoint=ProcessAudioView, methods=['POST']),
]