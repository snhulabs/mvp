import http
from starlette.exceptions import HTTPException


class BaseHTTPException(HTTPException):
	NAME = 0
	STATUS_CODE = 500
	DETAILS = None
	
	def __init__(self, *args, **kwargs):
		if self.DETAILS:
			details = '{} ({})'.format(self.DETAILS, self.NAME)
		else:
			details = '{} ({})'.format(
				http.HTTPStatus(self.STATUS_CODE).phrase)

		super().__init__(
			self.STATUS_CODE,
			details)


class InvalidToken(BaseHTTPException):
	NAME = 1
	STATUS_CODE = 401
	DETAILS = 'Invalid token'



