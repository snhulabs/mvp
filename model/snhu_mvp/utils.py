import os

from google.cloud import storage


def download_file(file_uri, download_to):
    client = storage.Client()
    bucket = client.bucket('[APP-ID-ON-GCP]-audio')
    blob = bucket.blob(os.path.basename(file_uri))
    blob.download_to_filename(download_to)

def upload_file(destination):
    client = storage.Client()
    bucket = client.bucket('[APP-ID-ON-GCP]-audio')
    blob = bucket.blob(os.path.basename(destination))
    uploaded = blob.upload_from_filename(destination)
    return os.path.join(
        *['[bucket-name]', os.path.basename(blob._properties['selfLink'])])

def run_smile(audio_file_path, csv_output_path, config_path, lld_output_path=None):
    smile_dir = os.path.join(*[os.path.dirname(__file__), 'opensmile'])
    smile_binary = f'{smile_dir}/build/progsrc/smilextract/SMILExtract'
    smile_conf = os.path.join(*[f'{smile_dir}/config', config_path])
    if lld_output_path:
        process_command = f'{smile_binary} -C {smile_conf} -I {audio_file_path} -O {csv_output_path} -lldcsvoutput, -D {lld_output_path} -nologfile'
    else:
        process_command = f'{smile_binary} -C {smile_conf} -I {audio_file_path} -O {csv_output_path} -nologfile'
    os.system(process_command)
