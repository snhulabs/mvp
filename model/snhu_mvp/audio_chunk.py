import numpy
from pydub import AudioSegment

def get_chunks(file_name, window_size=5, step_size=3):
    myaudio = AudioSegment.from_file(file_name , 'wav')
    chunk_file_names = []
    duration = int(numpy.floor(myaudio.duration_seconds*1000))
    window_size_msec = window_size*1000
    step_size_msec = step_size*1000
    for start in range(0, duration, step_size_msec):
        end = start + window_size_msec
        window = myaudio[start:end]
        chunk_file_name = file_name.replace('.wav', '') + '_' + str(int(start/1000)) + '_' + str(int(end/1000)) + '.wav'
        chunk_file_names.append(chunk_file_name)
        window.export(chunk_file_name, format="wav")
    return chunk_file_names