"""Main Class for inference

Requirements:
    * A static meta data file should be present. It can be updated.
    * meta data (*.json) has audio file link and necessary open-smile outputs
        - csvFile1: Output of the execution with IS13_ComParE.conf. 
                For this execution 3 parameters in FrameModeFunctionals.conf.inc 
                should be updated as seen below:
                frameMode = fixed
                frameSize = 5 
                frameStep = 3
        - csvFile2:Output of the execution with IS13_ComParE.conf with 
                -lldcsvoutput argument.
    * .pkl file for the model

"""

import os
import random
import requests
import pickle
import json
import shutil
from tempfile import NamedTemporaryFile


import librosa
import pandas as pd
import numpy as np
from pydub import AudioSegment
import parselmouth
from shutil import rmtree

from utils import timeit
from snhu_mvp.syllable_nuclei import speech_rate
from snhu_mvp.audio_chunk import get_chunks

FEATURES_NUM = 6375
OPENSMILE_TOP_BUFFER = 2
OPENSMILE_BOTTOM_BUFFER = 3
ROW_SKIP = FEATURES_NUM + OPENSMILE_TOP_BUFFER + OPENSMILE_BOTTOM_BUFFER


class InferenceProvider:

    def __init__(self, step_size=3, window_size=5):
        """Takes config details as param here.

        Configuration could  be read from the environment or a flat 
        file (e.g. config.txt).
        
        Args:
            step_size (int): step size parsing audio
            window_size (int): window size parsing audio
        """
        self.step_size = step_size
        self.window_size = window_size
        self.models = {
            'friendliness': pickle.load(open(os.path.join(*[os.path.dirname(__file__), 'input', 'friendliness_model.pkl']), 'rb')),
            'intelligence': pickle.load(open(os.path.join(*[os.path.dirname(__file__), 'input', 'intelligence_model.pkl']), 'rb')),
            'likability': pickle.load(open(os.path.join(*[os.path.dirname(__file__), 'input', 'likability_model.pkl']), 'rb')),
            'competence': pickle.load(open(os.path.join(*[os.path.dirname(__file__), 'input', 'competence_model.pkl']), 'rb')),
        }

    def load_model(self, key, filename):
        self.models[key] = pickle.load(open(filename, 'rb'))

    def run_predictions(self, csv_file):
        num_lines = sum(1 for line in open(csv_file))
        keep = list(range(OPENSMILE_TOP_BUFFER, OPENSMILE_TOP_BUFFER + FEATURES_NUM + 1))
        to_exclude = list(range(OPENSMILE_TOP_BUFFER)) + list(range(OPENSMILE_TOP_BUFFER + FEATURES_NUM + 1, num_lines))
        features_names = pd.read_csv(
            csv_file, 
            skiprows=to_exclude, 
            names=['features'],
            engine='python'
        ).features.tolist()

        df1 = pd.read_csv(csv_file, skiprows=ROW_SKIP, names=features_names)

        predictions = {}
        for name, model in self.models.items():
            prediction = model.predict_proba(df1)[:,1]
            predictions[name] = list(prediction)
            print(name, 'predictions are done')

        return predictions

    def get_speech_rate(self, audio_path, window_size=5, step_size=3):
        myaudio = AudioSegment.from_file(audio_path , 'wav')
        chunk_files = []
        duration = int(np.floor(myaudio.duration_seconds*1000))
        window_size_msec = window_size*1000
        step_size_msec = step_size*1000
        for start in range(0, duration, step_size_msec):
            end = start + window_size_msec
            window = myaudio[start:end]
            chunk_file = NamedTemporaryFile(suffix='.wav')
            chunk_files.append(chunk_file)
            window.export(chunk_file.name, format="wav")
        # assumption: Avg number of syllable in a word is 1.2 for English
        # speech rate not calculated for chunks less than 5 seconds / window size
        rates = []
        last_rate = None
        for f in chunk_files:
            try:
                rate = speech_rate(f.name)
                last_rate = rate
            except (parselmouth.PraatError, IndexError, ZeroDivisionError):
                rate = last_rate

            if rate:
                rates.append(rate)

        [i.close() for i in chunk_files]

        return [i['speechrate(nsyll / dur)'] * (60 / 1.2) for i in rates if i['dur(s)'] >= 5.0]


