import os
import uvicorn

from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

from snhu_mvp import settings
from snhu_mvp.routes import routes

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'gcp_key.json'

middleware = [
	Middleware(
		CORSMiddleware,
		allow_origins=[
			# 'http://localhost:3000'
			'*'
		],
		allow_headers=['*'],
		allow_methods=['*'])
]


app = Starlette(
	debug=True,
	middleware=middleware,
	routes=routes
)