import requests
import sys
import os


local_base_url = 'http://localhost:1337'

use_env = 'local' # 'dev'
base_url = local_base_url

#Download a file
def download_file(url, task_id, attempt, path):
    filename = "/task_{}_attempt_{}.wav".format(task_id, attempt)
    r = requests.get(url, headers=headers)
    with open(path+filename, 'wb') as f:
        f.write(r.content)

def download_tasks(items, task, user_id):
    path = os.path.join("./snhu", str(user_id), str(task))
    if not os.path.exists(path):
        os.mkdir(path)
    for item in items:
        url = item['audioFile']
        if url is not None and url != 'undefined':
            download_file(
                '{}/exportdata/{}'.format(base_url, item['id']),
                item['id'],
                item['attempt'],
                path
            )

#Get JWT Token
def request_jwt():
    req = requests.post(f'{base_url}/auth/local', data={'identifier': '', 'password': ''})
    result  = req.json()
    jwt = result['jwt']
    print("Your jwt is ", jwt)
    return jwt

token = request_jwt()

#Get export json
headers = {'Authorization': f'Bearer {token}'}
r = requests.get(base_url+'/exportdata', headers=headers)
content = r.json()

import json

with open(f'data_{use_env}.json', 'w') as f:
    json.dump(content, f)

print("** DONE ** ")
