import os, glob, pickle, time

def timeit(f):
	'''Measures how long a function call takes.'''
	def _f(*args, **kwargs):
		start = time.time()
		out = f(*args, **kwargs)
		msg = f.__name__ + ' took '
		msg += '{:.3f}s'.format(time.time()-start)
		print(msg)
		return out
	return _f