"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { Storage } = require("@google-cloud/storage");
const storage = new Storage();
const fs = require("fs-extra");
const os = require("os");

const path = require("path");
const jwt = require("jsonwebtoken");
const publicKey = fs.readFileSync(
  path.join(__dirname, "../utils/public.key"),
  "utf8"
);

//TODO get api based local or live

const getRetrieveTasksByName = (tasks, name) =>
  tasks.filter((task) => task.title === name);

const retrieveTaskData = (tasks, name) =>
  getRetrieveTasksByName(tasks, name).map((task) => {
    const baseUrl = process.env.CMS_GRAPHQL_URL
      ? process.env.CMS_GRAPHQL_URL.replace("/graphql")
      : "http://localhost:1337";

    return {
      id: task.id,
      name: task.title,
      date: task.date,
      original_text: task.text,
      modify_text: task.modifyText,
      attempt: task.attempt,
      audioFile: task.audioFile,
      confidence: task.confidence,
      words: task.words,
      smileData: task.smileData
    };
  });

const retrieveAudio = async (id, ctx) => {
  let task = await strapi.services["task"].find({ id: id });
  if (task.length == 1) {
    // Skip tasks without audio files.
    if (!task[0].audioFile) {
      return false;
    };
    const user_id = task[0]["app_user"]["id"];
    const mybucket = await storage.bucket("");
    const fileName = task[0].audioFile.split("/").pop();
    const destFilePath = os.tmpdir() + "/snhu/user" + user_id + "/";
    const destFile = destFilePath + fileName;
    // GCS requires the file exist before downloading it.
    fs.ensureFile(destFile);
    const file = mybucket.file(fileName);

    let file_exists = false;
    await file.exists().then(function (data) {
      file_exists = data[0];
    });

    if (file_exists) {
      async function downloadFile() {
        const options = {
          destination: destFile,
        };
        // Downloads the file
        await file.download(options);
      }
      await downloadFile().catch(console.error);

      ctx.response.statusCode = "200";
      ctx.response.set("Content-Type", "audio/wav");
      const contents = fs.readFileSync(destFile);
      // Cleans the tmp directory after completed file sync
      //fs.removeSync(destFilePath, {recursive: true});
      return contents;
    } else return false;
  } else return false;
};

module.exports = {
  async find(ctx) {
    let users = await strapi.services["app-users"].find({_limit: -1});

    const exportData = users.map((user) => {
      return {
        id: user.id,
        userId: user.userId,
        mic_permission: user.step >= 3,
        mic_extra: user.mic_extra,
        environment: user.environmentLocation,
        step: user.step,
        date: user.created_at,
        browserUserAgent: user.browser,
        deviceTypeUserAgent: user.device,
        tasks: {
          mic1: retrieveTaskData(user.tasks, "Mic Test 1"),
          mic2: retrieveTaskData(user.tasks, "Mic Test 2"),
          competence: retrieveTaskData(user.tasks, "Competence"),
          likeability: retrieveTaskData(user.tasks, "Likeability"),
          normal: retrieveTaskData(user.tasks, "PuttingItAllTogether"),
        },
      };
    });

    return JSON.stringify(exportData);
  },
  //Playback for the export
  async findOne(ctx) {
    if (ctx.params && ctx.params["id"]) {
      const id = ctx.params["id"];
      const contents = await retrieveAudio(id, ctx);
      const body = ctx.request.body;

      if (contents !== false) {
        return ctx.response.send(contents);
      } else return { error: "no file" };
    }
    return { error: "no file" };
  },

  // Playback for the website
  async count(ctx) {
    try {
      if (ctx.params && ctx.params["id"]) {
        const id = ctx.params["id"];
        const task = await strapi.services["task"].find({ id: id });
        const body = ctx.request.body;
        const verifiedToken = jwt.verify(body["token"], publicKey);
        if (task.length == 1 && verifiedToken) {
          const task_user_id = task[0]["app_user"]["id"];
          const user_id = verifiedToken["id"];

          if (task_user_id === user_id) {
            const contents = await retrieveAudio(id, ctx);
            if (contents !== false) {
              return ctx.response.send(contents);
            } else return { error: "no file" };
          } else return { error: "user not authorise" };
        } else return { error: "not authorise" };
      }
      return { error: "no file" };
    } catch (e) {
      return { error: "error processing request" };
    }
  },
};
