"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const wav = require("./lib/wav/");
const { Storage } = require("@google-cloud/storage");
const speech = require("@google-cloud/speech").v1p1beta1;

const storage = new Storage();
const client = new speech.SpeechClient();
const fs = require("fs");
const path = require("path");

const jwt = require("jsonwebtoken");
const publicKey = fs.readFileSync(
  path.join(__dirname, "../utils/public.key"),
  "utf8"
);

//Functions
const float32ToInt16 = (buffer) => {
  let l = buffer.length;
  let buf = new Int16Array(l);
  while (l--) {
    buf[l] = Math.min(1, buffer[l]) * 0x7fff;
  }
  console.log(buf.buffer);
  return buf.buffer;
};

const loadFile = (filename) => {
  let data = fs.readFileSync(filename);

  console.log(data);

  let buf = Buffer.from(data);
  let ab = new ArrayBuffer(buf.length);
  let view = new Uint8Array(ab);
  for (let i = 0; i < buf.length; ++i) {
    view[i] = buf[i];
  }
  return ab;
};

const parseAudioLong = async (fileName) => {
  // The audio file's encoding, sample rate in hertz, and BCP-47 language code
  const audio = {
    uri: "gs://snhu-data-collection-fc00-audio/" + fileName,
  };
  const config = {
    encoding: "LINEAR16",
    sampleRateHertz: 16000,
    languageCode: "en-US",
    enableAutomaticPunctuation: true,
  };
  const request = {
    audio: audio,
    config: config,
  };

  // Detects speech in the audio file
  try {
    const [operation] = await client.longRunningRecognize(request);
    const [response] = await operation.promise();
    const results = response.results.map((result) => console.log(result));

    const transcription = response.results
      .map((result) => result.alternatives[0].transcript)
      .join("\n");
    console.log(`Transcription: ${transcription}`);
    return transcription;
  } catch (e) {
    console.log(e);
  }
};

const parseAudio = async (fileName) => {
  const file = fs.readFileSync(fileName);
  const audioBytes = file.toString("base64");
  // The audio file's encoding, sample rate in hertz, and BCP-47 language code
  const audio = {
    content: audioBytes,
  };
  const config = {
    encoding: "LINEAR16",
    sampleRateHertz: 16000,
    languageCode: "en-US",
    enableAutomaticPunctuation: true,
  };
  const request = {
    audio: audio,
    config: config,
  };

  // Detects speech in the audio file
  try {
    const [response] = await client.recognize(request);
    const results = response.results.map((result) => console.log(result));

    const transcription = response.results
      .map((result) => {
        console.log(result.alternatives);
        return result.alternatives[0].transcript;
      })
      .join("\n");
    console.log(`Transcription: ${transcription}`);
    return transcription;
  } catch (e) {
    console.log(e);
  }
};

const tmp_folder = "/tmp/snhu/";
const zeroPad = (num, places) => String(num).padStart(places, "0");

module.exports = {
  async count(ctx) {
    let body = ctx.request.body;

    const verifiedToken = jwt.verify(body["token"], publicKey);
    if (verifiedToken) {
      const user_id = verifiedToken["id"];
      const task_id = ctx.params["id"];
      const filename = "output_user_" + user_id + "_task_" + task_id + ".wav";
      //Getting my bucket
      const mybucket = await storage.bucket("");
      try {
        //Convert binary to wav
        let fileWriter = new wav.FileWriter(
          tmp_folder + "user" + user_id + "/" + filename,
          {
            channels: 1,
            sampleRate: 16000,
            bitDepth: 16,
            dataLength: 0,
          }
        );
        //combine all the binary frames into wav
        fs.readdirSync(tmp_folder + "user" + user_id).forEach((file) => {
          if (file.indexOf("out") >= 0) {
            console.log(file);
            let dataFile = loadFile(
              tmp_folder + "/user" + user_id + "/" + file
            );
            fileWriter.write(new Buffer(dataFile));
          }
        });
        //close file
        fileWriter.end();

        //Upload wav to the bucket
        const file = await mybucket.upload(
          tmp_folder + "user" + user_id + "/" + filename
        );
        //Parse audio to google sppech
        /* const output = await parseAudio(
          tmp_folder + "user" + user_id + "/" + filename
        );*/
        const output = await parseAudioLong(filename);

        /*fs.readdir("/tmp/snhu/user" + id, (err, files) => {
          if (err) return;
          for (const file of files) {
            fs.unlink(path.join("/tmp/snhu/user" + id, file), (err) => {
              if (err) return;
            });
          }
        });*/
        // fs.rmdirSync("/tmp/snhu/user" + id, { recursive: true }); // might not need the top part probably
        return {
          text: output,
          user: user_id,
          task: task_id,
          audio: filename,
        };
      } catch (e) {
        console.log("error read", e);
        return { error: "unable to parse audio" };
      }
    }
  },
  async update(ctx) {
    //Add extra data to the binary file
    let body = ctx.request.body;
    let size = Object.keys(body["data"]).length;

    if (body["data"]) {
      const verifiedToken = jwt.verify(body["token"], publicKey);
      if (verifiedToken) {
        const user_id = verifiedToken["id"];
        const task_id = ctx.params["id"];

        let size = Object.keys(body["data"]).length;
        let data = new Float32Array(size);
        for (let i = 0; i < size; i++) {
          data[i] = body["data"][i];
        }
        const index = body["index"];

        let all = fs.createWriteStream(
          tmp_folder + "user" + user_id + "/out" + zeroPad(index, 5),
          {
            flags: "w",
          }
        );
        let dataConverted = float32ToInt16(data);

        all.write(new Buffer(dataConverted));
        all.end();
        return { status: true, size: size, user: user_id, task: task_id };
      } else return { error: "not valid token" };
    } else return { error: "no data in body" };
  },
  async find(ctx) {
    if (!fs.existsSync(tmp_folder)) {
      fs.mkdirSync(tmp_folder);
    }

    let body = ctx.request.body;
    const verifiedToken = jwt.verify(body["token"], publicKey);
    if (verifiedToken) {
      const user_id = verifiedToken["id"];

      if (!fs.existsSync(tmp_folder + "user" + user_id)) {
        // If doesnt exists.. well create one
        fs.mkdirSync(tmp_folder + "user" + user_id);
      } else {
        //Make sure it is empty
        fs.readdir(tmp_folder + "user" + user_id, (err, files) => {
          if (err) return;
          for (const file of files) {
            fs.unlink(path.join(tmp_folder + "user" + user_id, file), (err) => {
              if (err) return;
            });
          }
        });
      }
      return { status: "started", id: user_id };
    } else return { error: "not valid token" };
    return { status: "failed token" };
  },
};
