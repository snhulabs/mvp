'use strict';

const audioToSpeech = require('../utils/audioToSpeech');
const processWav = require('../utils/processWav');
const upload = require('../utils/upload');
const getSmileData = require('../utils/smileData');

module.exports = {
  async register(ctx) {
    const audioFile = ctx.request.files['audio_data'];
    const audioDuration = ctx.request.body['duration'];
    const speechContext = ctx.request.body['speech_context'];
    const fileName = audioFile.path;
    await processWav(fileName);
    const fileURI = await upload(fileName);
    const results = await audioToSpeech(fileName, fileURI, audioDuration, speechContext);
    const smileData = await getSmileData(fileURI);
    return {
      status: 200,
      data: {
        transcription: results.transcription.join(' '),
        confidence: results.confidence,
        words: results.words,
        fileURI: fileURI,
        smileData: smileData
      },
    };
  },
};
