const FileSystem = require("fs").promises;
const WaveFile = require('wavefile').WaveFile;

const SAMPLE_RATE_HERTZ = 16000;

module.exports = async function processWav(fileName) {
  const file = await FileSystem.readFile(fileName);
  let wav = new WaveFile(file);
  wav.toSampleRate(SAMPLE_RATE_HERTZ);
  return FileSystem.writeFile(fileName, wav.toBuffer());
}

