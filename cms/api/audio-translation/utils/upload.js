'use strict';

const { Storage } = require('@google-cloud/storage');

const GC_STORAGE_BUCKET = '';

module.exports = async function upload(fileName) {
  const storage = new Storage();
  const bucket = await storage.bucket(GC_STORAGE_BUCKET);
  const [uploadResponse] = await bucket.upload(fileName, {
    destination: `${(new Date()).getTime()}.wav`,
  });

  return `gs://${GC_STORAGE_BUCKET}/${uploadResponse.metadata.name}`;
};
