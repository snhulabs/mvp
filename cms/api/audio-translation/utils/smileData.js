'use strict';

const axios = require('axios');

module.exports = async function getSmileData(fileURI) {
  const response = await axios.post('https://snhu-backend-ufm7glmqja-ew.a.run.app/process_audio',
    {
      'fileURI': fileURI
    },
    {
      headers: {
        'Content-Type': 'application/json',
      }
    }
  );

  return response.data;
};
