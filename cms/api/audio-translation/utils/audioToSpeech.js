'use strict';

const FileSystem = require("fs").promises;
const speech = require("@google-cloud/speech").v1p1beta1;

const AUTOMATIC_PUNCTATION = true;
const FILE_ENCODING = 'base64';
const LANG = 'en-US';
const RECOGNIZE_ENCODING = 'LINEAR16';
const SAMPLE_RATE_HERTZ = 16000;
const TIME_OFFSET = 60;
const ENABLE_WORD_CONFIDENCE = true;
const ENABLE_WORD_TIME_OFFSET = true;
const MODEL = 'video';

module.exports = async function audioToSpeech(fileName, fileURI, audioDuration, speechContext) {
  const client = new speech.SpeechClient();
  let audio = {};

  if (parseInt(audioDuration, 10) < TIME_OFFSET) {
    const encodedFile = await FileSystem.readFile(fileName, {
      encoding: FILE_ENCODING,
    });
    
    audio = {
      content: encodedFile.toString(FILE_ENCODING),
    };
  } else {
    audio = {
      uri: fileURI,
    };
  }
  
  const recognizeRequest = {
    config: {
      enableAutomaticPunctuation: AUTOMATIC_PUNCTATION,
      encoding: RECOGNIZE_ENCODING,
      languageCode: LANG,
      sampleRateHertz: SAMPLE_RATE_HERTZ,
      enableWordConfidence: ENABLE_WORD_CONFIDENCE,
      enableWordTimeOffsets: ENABLE_WORD_TIME_OFFSET,
      model: MODEL,
    },
    audio,
  };

  if (speechContext) {
    const phrases = [];
    const tmp = [];

    speechContext.split(' ').forEach((phrase, index) => {
      tmp.push(phrase);

      if (index % 5 === 0) {
        phrases.push(phrase);
        tmp.length = 0;
      }
    });

    recognizeRequest.config.speechContexts = [{
      phrases,
    }];
  }

  const [operation] = await client.longRunningRecognize(recognizeRequest);
  const [recognizeResponse] = await operation.promise();
  const transcription = recognizeResponse.results.map(result =>
    result.alternatives[0].transcript,
  );

  const confidence = recognizeResponse.results.map(result => 
    result.alternatives[0].confidence).join('\n');

  const words = recognizeResponse.results.map(result => result.alternatives[0]);

  return {
    'transcription': transcription,
    'confidence': confidence,
    'words': words.map(word => word.words)[0],
  };
};
