const fs = require("fs");
const path = require("path");
const jwt = require("jsonwebtoken");

const privateKey = fs.readFileSync(
  path.join(__dirname, "../utils/private.key"),
  "utf8"
);

module.exports = {
  definition: `
    extend type createAppUserPayload {
      token:String
      id:Int
    }
    extend input editAppUserInput {
      token:String
      id: Int
    }
  `,
  resolver: {
    Mutation: {
      createAppUser: {
        description: "Create App User",
        resolverOf: "application::app-users.app-users.create",
        resolver: async (obj, options, ctx) => {
          const appUser = await strapi.controllers["app-users"].create(
            ctx.context
          );

          console.log(appUser);
          const token = jwt.sign(
            { userId: appUser.userId, id: appUser.id },
            privateKey,
            {
              algorithm: "RS256",
              expiresIn: "1d",
            }
          );

          let id = appUser.userId;
          return {
            appUser,
            token,
            id,
          };
        },
      },
    },
  },
};
