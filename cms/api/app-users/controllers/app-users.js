'use strict';

const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const { sanitizeEntity } = require('strapi-utils');

const publicKey = fs.readFileSync(path.join(__dirname, '../utils/public.key'), 'utf8');

module.exports = {
  async create(ctx) {
    const entity = await strapi.services['app-users'].create(ctx.request.body)
    
    return await sanitizeEntity(entity, { model: strapi.models['app-users'] });
  },
  async update(ctx) {
    const { token, ...data } = ctx.request.body;
    const verifiedToken = jwt.verify(token, publicKey);
    
    if (verifiedToken && verifiedToken.userId) {
      const appUserID = ctx.params.id;
      
      const entity = await strapi.services['app-users'].update({ id: appUserID }, data);

      return sanitizeEntity(entity, { model: strapi.models['app-users'] });
    }

    return null;
  }
};
