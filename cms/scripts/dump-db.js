const mysqldump = require('mysqldump');

mysqldump({
  connection: {
    host: 'localhost',
    port: 3308,
    user: 'root',
    password: 'password',
    database: 'cms'
  },
  dumpToFile: './init.sql',
});
