"use strict";

const { exec } = require("child_process");
const axios = require("axios");

/**
 * commit-db.js controller
 *
 * @description: A set of functions called "actions" of the `commit-db` plugin.
 */

module.exports = {
  /**
   * Default action.
   *
   * @return {Object}
   */

  index: async (ctx) => {
    if (process.env.BITBUCKET_USERNAME && process.env.BITBUCKET_PASSWORD) {
      // Trigger pipeline
      const workspace = process.env.BITBUCKET_REPOSITORY_OWNER;
      const repoSlug = process.env.BITBUCKET_REPOSITORY_SLUG;
      const user = process.env.BITBUCKET_USERNAME;
      const pass = process.env.BITBUCKET_PASSWORD;
      const url = `https://${user}:${pass}@api.bitbucket.org/2.0/repositories/${workspace}/${repoSlug}/pipelines/`;

      axios
        .post(url, {
          target: {
            type: "pipeline_commit_target",
            commit: {
              hash: process.env.BITBUCKET_COMMIT,
              type: "commit"
            },
            selector: {
              type: "custom",
              pattern: `db-update-${process.env.ENV}`,
            },
          },
        })
        .then((res) => {
          console.log(`Pipeline trigger success, statusCode: ${res.statusCode}`);
        })
        .catch((error) => {
          console.error('Pipeline trigger error');
          console.error(error);
        });
    } else {
      exec("npm run dump-db", (err, stdout, stderr) => {
        if (err) {
          console.error(err);
          return;
        }
      });
    }

    ctx.send({
      ok: true,
    });
  },
};
