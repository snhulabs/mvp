// const fs = require('fs');
// const path = require('path');

// const projectId = process.env.GCLOUD_PROJECT_ID;
// const bucketName = `${projectId}-uploads`;
// let serviceAccountString = fs.readFileSync(path.join(__dirname, '../.uploader-service-account-key.json')).toString();

module.exports = {
  graphql: {
    endpoint: '/graphql',
    tracing: true,
    shadowCRUD: true,
    playgroundAlways: false,
    depthLimit: 7,
    amountLimit: 100,
  },
  // upload: {
  //   provider: 'google-cloud-storage',
  //   providerOptions: {
  //     serviceAccount: serviceAccountString,
  //     bucketName: bucketName,
  //     sizeLimit: 50000,
  //     enabled: true,
  //     baseUrl: 'https://storage.googleapis.com/{bucket-name}',
  //     publicFiles: true,
  //   },
  // },
};
