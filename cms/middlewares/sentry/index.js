const Sentry = require('@sentry/node');

if (process.env.SENTRY_CMS_DSN) {
  Sentry.init({
    dsn: process.env.SENTRY_CMS_DSN,
    environment: strapi.config.environment,
    release: process.env.BITBUCKET_BUILD_NUMBER,
  });
}

module.exports = strapi => {
  return {
    initialize() {
      if (process.env.SENTRY_CMS_DSN) {
        strapi.app.use(async (ctx, next) => {
          try {
            await next();
          } catch (error) {
            Sentry.captureException(error);
            throw error;
          }
        });
      }
    },
  };
};
