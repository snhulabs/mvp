import React, { useEffect, useState } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import Title from 'components/Title';
import PageContent from 'components/PageContent';
import ActionButton from 'components/ActionButton';

import TryAgainButton from './styled/TryAgainButton';
import Buttons from './styled/Buttons';

import EditorInterview from 'components/EditorInterview';
import { useLocalizedRouter } from '../../hooks';
import { AppUserContext } from '../../context/appUser.context';

import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface SubmitPageProps {
  copy: Object;
}

const SubmitPage: React.FunctionComponent<SubmitPageProps> = ({
  copy,
  ...restProps
}) => {
  const { update } = React.useContext(AppUserContext);

  const router = useLocalizedRouter();

  const onTryAgain = () => {};
  const onSubmit = () => {
    /*  update({ environmentLocation }, () => {
      router.push('/setup');
    });
    */

    router.push('/end');
  };
  const [text, setText] = useState('');

  useEffect(() => {
    setText(localStorage.getItem('text'));
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <PageContent>
        <Title>{copy["app.submitPage.title"]}</Title>
        <div>
          <EditorInterview text={text}></EditorInterview>
        </div>
        <Buttons>
          <TryAgainButton onClick={onTryAgain}>{copy["app.submitPage.tryAgainButton"]}</TryAgainButton>
          <ActionButton onClick={onSubmit}>{copy["app.submitPage.actionButton"]}</ActionButton>
        </Buttons>
      </PageContent>
    </motion.div>
  );
};

export default SubmitPage;
