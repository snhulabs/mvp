import React, { useState } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import { useLocalizedRouter } from 'hooks';

import Title from 'components/Title';
import LoginForm from 'components/LoginForm';
import QuestionaryButton from 'components/QuestionaryButton';
import Description from 'components/Description';
import ActionButton from 'components/ActionButton';

import PageContent from './styled/PageContent';
import QuestionaryWrapper from './styled/QuestionaryWrapper';
import QuestionaryOptions from './styled/QuestionaryOptions';
import { AppUserContext } from 'context/appUser.context';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';
import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';
import parse from 'html-react-parser';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
        questionaryData: copyResults.data.copies[0].copies["app.loginPage.questionaryData"]
      },
      revalidate: 10,
    })
  );

interface LoginPageProps {
  copy: Object;
  questionaryData: Array<any>;
}

const LoginPage: React.FunctionComponent<LoginPageProps> = ({
  copy,
  questionaryData,
  ...restProps
}) => {
  const { goTo, setupView } = React.useContext(NavigationContext);
  const { update } = React.useContext(AppUserContext);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [selectedQuestionaryOption, setSelectedQuestionaryOption] = useState(
    null
  );
  const router = useLocalizedRouter();

  const onLoginCallback = () => {
    setIsLoggedIn(true);
  };

  const onProceed = () => {
    const environmentLocation =
      questionaryData[selectedQuestionaryOption].text || '';

    update({ environmentLocation, step: 2 }, () => {
      goTo(ROUTING_STEPS.SETUP_MIC);
    });
  };

  const selectOption = option => () => {
    setSelectedQuestionaryOption(option);
  };

  React.useEffect(() => {
    setupView(ROUTING_STEPS.ENTER_USER_ID);
  }, []);

  const renderLoginForm = () => (
    <PageContent>
      <Title>{copy["app.loginPage.form.title"]}</Title>
      <LoginForm onLoginCallback={onLoginCallback} />
    </PageContent>
  );

  const renderOptions = () =>
    questionaryData.map(option => (
      <QuestionaryButton
        selected={option.idx === selectedQuestionaryOption}
        key={option.idx}
        onClick={selectOption(option.idx)}
      >
        {option.text}
      </QuestionaryButton>
    ));

  const renderQuestionary = () => (
    <PageContent>
      <Title>{copy["app.loginPage.questionary.title"]}</Title>
      <QuestionaryWrapper>
        <Description>
          {parse(copy["app.loginPage.questionary.description"])}
        </Description>
        <QuestionaryOptions>{renderOptions()}</QuestionaryOptions>
      </QuestionaryWrapper>
      <ActionButton
        onClick={onProceed}
        disabled={selectedQuestionaryOption === null}
      >
        {copy["app.loginPage.questionary.actionButton"]}
      </ActionButton>
    </PageContent>
  );

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      {isLoggedIn ? renderQuestionary() : renderLoginForm()}
    </motion.div>
  );
};

export default LoginPage;
