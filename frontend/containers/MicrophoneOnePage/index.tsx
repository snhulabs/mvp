import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import Title from 'components/Title';
import PageContent from 'components/PageContent';
import ActionButton from 'components/ActionButton';
import { AudioContext } from 'context/audio.context';
import { AppUserContext } from 'context/appUser.context';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';
import MicrophoneOne from 'components/MicrophoneOne';
import IntroMic from 'components/IntroMic';
import { MIC_ONE_LENGTH } from 'config';

import Microphone from './styled/Microphone';
import Success from './styled/Success';
import renderMicProgress, { PROGRESS } from 'utils/renderMicProgress';
import { createTask, updateTask } from 'utils/api/recording';
import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface MicrophoneOnePageProps {
  copy: Object;
}

const MicrophoneOnePage: React.FunctionComponent<MicrophoneOnePageProps> = ({ copy }) => {
  const redirectTimeout = React.useRef(null);
  const [isCountdown, setIsCountdown] = React.useState<boolean>(false);
  const [task, setTask] = React.useState(null);
  const [attempt, setAttempt] = React.useState<number>(1);
  const [progress, setProgress] = React.useState<PROGRESS>(PROGRESS.INTRO);
  const { goTo, setupView } = React.useContext(NavigationContext);
  const { update, token, entryId } = React.useContext(AppUserContext);
  const { microphoneEnabled } = React.useContext(AudioContext);

  const onProceed = () => {
    setIsCountdown(true);
    setProgress(PROGRESS.MIC_RECORD);
  };

  const onUploadFinish = async (audioURL, transcription, confidence, words) => {
    await updateTask({
      audioFile: audioURL,
      entryId: task.id,
      text: transcription,
      modifyText: '',
      confidence: confidence,
      words: words,
      smileData: [],
      token: token,
    });

    setIsCountdown(false);
    setProgress(PROGRESS.SUCCESS);

    redirectTimeout.current = setTimeout(() => {
      update({ step: 4 }, () => {
        goTo(ROUTING_STEPS.MIC_TWO);
      });
    }, 5000);
  };

  React.useEffect(() => {
    setupView(ROUTING_STEPS.MIC_ONE);

    createTask({
      attempt,
      title: 'Mic Test 1',
      entryId,
      token,
    }).then(task => {
      setTask(task);
    });

    return () => {
      clearTimeout(redirectTimeout.current);
    };
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        <Title>{copy["app.micOnePage.title"]}</Title>
        <Microphone
          isActive={progress === PROGRESS.MIC_RECORD}
          isChecked={progress === PROGRESS.SUCCESS}
          isCountdown={isCountdown}
          time={MIC_ONE_LENGTH}
        />
        {microphoneEnabled &&
          renderMicProgress(progress, {
            [PROGRESS.INTRO]: (
              <>
                <IntroMic copy={copy["app.micOnePage.introMic"]} />
                <ActionButton onClick={onProceed}>{copy["app.micOnePage.actionButton"]}</ActionButton>
              </>
            ),
            [PROGRESS.MIC_RECORD]: (
              <MicrophoneOne onUploadFinish={onUploadFinish} copy={copy["app.micOnePage.microphoneOne"]} />
            ),
            [PROGRESS.SUCCESS]: (
              <Success>{copy["app.micOnePage.success"]}</Success>
            ),
          })}
      </PageContent>
    </motion.div>
  );
};

export default MicrophoneOnePage;
