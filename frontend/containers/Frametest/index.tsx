import React from 'react';
import { GetStaticProps } from 'next';

import Container from './styled/Container';
import Iframe from './styled/Iframe';

export const getStaticProps: GetStaticProps = async context => {
  return new Promise(resolve => {
    resolve();
  }).then(() => ({ props: {} }));
};

interface FrametestProps {}

const Frametest: React.FunctionComponent<FrametestProps> = () => (
  <Container>
    <Iframe
      allow="microphone"
      src=""
      height="900"
      width="1200"
    />
  </Container>
);

export default Frametest;
