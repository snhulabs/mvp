import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import PagePlanWrapper from 'components/Common/PagePlanWrapper';
import PageFirstPlan from 'components/Common/PageFirstPlan';
import PageSecondPlan from 'components/Common/PageSecondPlan';
import PageContent from 'components/PageContent';
import Title from 'components/Title';
import TaskTranscript from 'components/TaskTranscript';
import TaskRecording from 'components/TaskRecording';
import { LIKEABILITY_LENGTH, MAX_ATTEMPTS } from 'config';
import SuccessScreen from 'components/SuccessScreen';
import { NavigationContext } from 'context/navigation.context';
import PageThirdPlan from 'components/Common/PageThirdPlan';
import useTask from 'hooks/useTask';
import SPEECH_CONTEXT from 'hooks/useTask/speechContext.const';

import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';
import parse from 'html-react-parser';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface LikeabilityPageProps {
  copy: Object;
}

const copy = {
  taskDescription:`
     Sounding likeable means other perceive you as friendly, caring, and warm. This is exactly how you want to sound to a loved one going
     through a tough time! Imagine you are leaving a voicemail for an elderly family member who is sick. 
     Make a 1-minute recording addressing the following: `,
  steps: [
    {
      title: 'GREETING',
      details: 'Be as friendly as possible while greeting your relative.',
    },
    {
      title: 'APPRECIATION',
      details:
        'Be as warm as possible while explaining what people appreciate about them.',
    },
    {
      title: 'HELP',
      details:
        'Be as caring as possible while you ask about and suggest ways to help.',
    },
  ],
};

const LikeabilityPage: React.FunctionComponent<LikeabilityPageProps> = ({ copy }) => {
  const { goToTask } = React.useContext(NavigationContext);

  const {
    audioContext,
    inputSource,
    attempt,
    audioSrc,
    isProcessing,
    isRecording,
    onRetry,
    onSubmit,
    task,
    transcription,
    confidence,
    words,
    smileData,
    showSuccess,
    startRecording,
    stopRecording,
    canTryAgain,
    upload,
  } = useTask('Likeability');

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        <Title>{copy["app.likeabilityPage.title"]}</Title>

        <PagePlanWrapper>
          <PageFirstPlan visible={!transcription && !showSuccess}>
            <TaskRecording
              onStartRecording={startRecording}
              onStopRecording={stopRecording}
              isProcessing={isProcessing}
              isRecording={isRecording}
              timeout={LIKEABILITY_LENGTH}
              copy={copy["app.likeabilityPage.copy"]}
              key={`task-recording-${attempt}`}
              audioContext={audioContext}
              inputSource={inputSource}
            />
          </PageFirstPlan>

          <PageSecondPlan visible={!showSuccess && transcription}>
            <TaskTranscript
              audioSrc={audioSrc}
              transcription={transcription}
              smileData={smileData}
              onSubmit={onSubmit}
              onRetry={onRetry}
              upload={upload}
              attempt={attempt || MAX_ATTEMPTS}
              canTryAgain={canTryAgain}
              key={`task-transcript-${attempt}`}
              speechContext={SPEECH_CONTEXT.LIKEABILITY}
              isVisible={!showSuccess && transcription != null}
            />
          </PageSecondPlan>

          <PageThirdPlan visible={showSuccess && transcription}>
            <SuccessScreen copy={copy["app.likeabilityPage.successScreen"]} onNext={goToTask} />
          </PageThirdPlan>
        </PagePlanWrapper>
      </PageContent>
    </motion.div>
  );
};

export default LikeabilityPage;
