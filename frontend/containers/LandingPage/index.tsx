import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import Wrapper from './styled/Wrapper';
import LeftPanel from './styled/LeftPanel';
import Section from './styled/Section';
import InfoPanel from './styled/InfoPanel';
import Title from './styled/Title';
import Logo from './styled/Logo';
import InfoTitle from './styled/InfoTitle';
import InfoDescription from './styled/InfoDescription';
import ActionButton from './styled/ActionButton';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';
import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';
import parse from 'html-react-parser';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );
;

interface LandingPageProps {
  copy: Object;
}

const LandingPage: React.FunctionComponent<LandingPageProps> = ({
  copy,
  ...restProps
}) => {
  const { goTo } = React.useContext(NavigationContext);

  const onProceed = () => {
    goTo(ROUTING_STEPS.ENTER_USER_ID);
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <Wrapper>
        <LeftPanel>
          <Logo />
        </LeftPanel>
        <Section>
          <Title>
            {parse(copy["app.landingPage.title"])}
            {/* Southern
            <br />
            New Hampshire
            <br />
            University */}
          </Title>
          <InfoPanel>
            <InfoTitle>
              {parse(copy["app.landingPage.infoTitle"])}
            </InfoTitle>
            <InfoDescription>
              <p>
                {parse(copy["app.landingPage.infoDescription.top"])}
              </p>
              <p>{copy["app.landingPage.infoDescription.bottom"]}</p>
            </InfoDescription>
            <ActionButton onClick={onProceed}>
              {parse(copy["app.landingPage.actionButton"])}
            </ActionButton>
          </InfoPanel>
        </Section>
      </Wrapper>
    </motion.div>
  );
};

export default LandingPage;
