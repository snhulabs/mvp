import styled from 'styled-components';
import Logo from 'components/Logo';

export default styled(Logo)`
  width: 50%;
`;
