import styled from 'styled-components';

export default styled.div`
  display: flex;
  justify-content: center;

  width: 15vw;
  height: 100%;

  padding-top: 5vh;
`;
