import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  padding: 0 15%;
  margin: 4vh 0;

  font-family: Arial, Helvetica, sans-serif;
  font-weight: 400;
  font-size: 1.2vw;

  border-left: 1px solid ${({ theme }) => theme.colors.gray};

  p {
    &:first-child {
      margin-top: 0;
    }
    &:last-child {
      margin-bottom: 0;
    }
  }
`;
