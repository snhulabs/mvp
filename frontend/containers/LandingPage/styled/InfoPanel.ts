import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;

  padding: 7%;
  width: 50%;

  background-color: ${({ theme }) => theme.colors.white};
`;
