import styled from 'styled-components';

export default styled.div`
  color: ${({ theme }) => theme.colors.white};
  font-size: 4vw;
  letter-spacing: 0.025em;
`;
