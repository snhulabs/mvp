import styled from 'styled-components';

export default styled.div`
  font-weight: 700;
  font-size: 1.5vw;
`;
