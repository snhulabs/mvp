import styled from 'styled-components';
import ActionButton from 'components/ActionButton';

export default styled(ActionButton)`
  margin-top: 4vh;
  margin-bottom: 0;
  width: 100%;
`;
