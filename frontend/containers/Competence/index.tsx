import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import PageContent from 'components/PageContent';
import TaskRecording from 'components/TaskRecording';
import Title from 'components/Title';
import TaskTranscript from 'components/TaskTranscript';
import PagePlanWrapper from 'components/Common/PagePlanWrapper';
import PageFirstPlan from 'components/Common/PageFirstPlan';
import PageSecondPlan from 'components/Common/PageSecondPlan';
import { COMPETENCE_LENGTH, MAX_ATTEMPTS } from 'config';
import { NavigationContext } from 'context/navigation.context';
import PageThirdPlan from 'components/Common/PageThirdPlan';
import SuccessScreen from 'components/SuccessScreen';
import useTask from 'hooks/useTask';
import SPEECH_CONTEXT from 'hooks/useTask/speechContext.const';
import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface CompetencePageProps {
  copy: Object;
}

const CompetencePage: React.FunctionComponent<CompetencePageProps> = ({ copy }) => {
  const { goToTask } = React.useContext(NavigationContext);

  const {
    audioContext,
    inputSource,
    attempt,
    audioSrc,
    isProcessing,
    isRecording,
    onRetry,
    onSubmit,
    task,
    transcription,
    confidence,
    words,
    smileData,
    showSuccess,
    startRecording,
    stopRecording,
    canTryAgain,
    upload,
  } = useTask('Competence');

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        <Title>{copy["app.competencePage.title"]}</Title>

        <PagePlanWrapper>
          <PageFirstPlan visible={!transcription && !showSuccess}>
            <TaskRecording
              onStartRecording={startRecording}
              onStopRecording={stopRecording}
              isProcessing={isProcessing}
              isRecording={isRecording}
              timeout={COMPETENCE_LENGTH}
              copy={copy["app.competencePage.copy"]}
              key={`task-recording-${attempt}`}
              audioContext={audioContext}
              inputSource={inputSource}
            />
          </PageFirstPlan>

          <PageSecondPlan visible={!showSuccess && transcription}>
            <TaskTranscript
              attempt={attempt || MAX_ATTEMPTS}
              audioSrc={audioSrc}
              transcription={transcription}
              smileData={smileData}
              canTryAgain={canTryAgain}
              onSubmit={onSubmit}
              onRetry={onRetry}
              upload={upload}
              key={`task-transcript-${attempt}`}
              speechContext={SPEECH_CONTEXT.COMPETENCE}
              isVisible={!showSuccess && transcription != null}
            />
          </PageSecondPlan>

          <PageThirdPlan visible={showSuccess && transcription}>
            <SuccessScreen copy={copy["app.competencePage.successScreen"]} onNext={goToTask} />
          </PageThirdPlan>
        </PagePlanWrapper>
      </PageContent>
    </motion.div>
  );
};

export default CompetencePage;
