import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import { MAX_ATTEMPTS } from 'config';

import TryAgainPopup from 'components/TryAgainPopup';
import Title from 'components/Title';
import PageContent from 'components/PageContent';
import ActionButton from 'components/ActionButton';
import MicrophoneTwo from 'components/MicrophoneTwo';
import SuccessScreen from 'components/SuccessScreen';

import { AudioContext } from 'context/audio.context';
import { AppUserContext } from 'context/appUser.context';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';

import Controls from './styled/Controls';
import TryAgainButton from './styled/TryAgainButton';
import { PROGRESS } from 'utils/renderMicProgress';
import { createTask, updateTask } from 'utils/api/recording';

import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';
import parse from 'html-react-parser';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface MicrophoneTwoPageProps {
  copy: Object;
}

const TASK_NAME = 'Mic Test 2';

const MicrophoneTwoPage: React.FunctionComponent<MicrophoneTwoPageProps> =
  ({ copy }) => {
    const [isCountdown, setIsCountdown] = React.useState<boolean>(false);
    const [task, setTask] = React.useState(null);
    const [attempt, setAttempt] = React.useState<number>(1);
    const [progress, setProgress] = React.useState(PROGRESS.INTRO);
    const [tryAgainOpen, setTryAgainPopup] = React.useState(false);
    const { goTo, setupView } = React.useContext(NavigationContext);
    const { update, token, entryId } = React.useContext(AppUserContext);
    const { microphoneEnabled } = React.useContext(AudioContext);

    const onProceed = () => {
      setIsCountdown(true);
      setProgress(PROGRESS.MIC_RECORD);
    };

    const onUploadFinish = async (
      audioURL,
      transcription,
      confidence,
      words
    ) => {
      await updateTask({
        audioFile: audioURL,
        entryId: task.id,
        text: transcription,
        confidence: confidence,
        words: words,
        smileData: [],
        modifyText: '',
        token,
      });

      setIsCountdown(false);
      setProgress(PROGRESS.SUCCESS);
    };

    const onSubmitTranscript = () => {
      setProgress(PROGRESS.CONFIRM_SUCCESS);
    };

    const onNext = () => {
      update({ step: 4 }, () => {
        const nextRoutes = [
          ROUTING_STEPS.COMPETENCE,
          ROUTING_STEPS.LIKEABILITY,
        ];

        goTo(nextRoutes[Math.floor(Math.random() * 2)]);
      });
    };

    const canTryAgain = () => !!(MAX_ATTEMPTS - (task?.attempt || 0));

    const onTryAgain = async () => {
      const newAttempt = attempt + 1;
      setProgress(PROGRESS.INTRO);
      setTryAgainPopup(false);
      setIsCountdown(false);
      setTask(null);
      setAttempt(newAttempt);

      createTask({
        attempt: newAttempt,
        title: TASK_NAME,
        entryId,
        token,
      }).then(task => {
        setTask(task);
      });
    };

    React.useEffect(() => {
      setupView(ROUTING_STEPS.MIC_TWO);

      createTask({
        attempt,
        title: TASK_NAME,
        entryId,
        token,
      }).then(task => {
        setTask(task);
      });
    }, []);

    const renderProgress = () => {
      switch (progress) {
        case PROGRESS.CONFIRM_SUCCESS:
          return <SuccessScreen copy={copy["app.micTwoPage.successScreen"]}  onNext={onNext} message />;
        default:
          return <MicrophoneTwo
            copy={copy["app.micTwoPage.microphoneTwo"]}
            onUploadFinish={onUploadFinish}
            progress={progress}
            isCountdown={isCountdown} />;
      }
    };

    const renderActions = () => {
      switch (progress) {
        case PROGRESS.INTRO:
          return <ActionButton onClick={onProceed}>{copy["app.micTwoPage.actionButtonIntro"]}</ActionButton>;
        case PROGRESS.SUCCESS:
          return (
            <>
              {canTryAgain() && (
                <TryAgainButton onClick={toggleTryAgainPopup}>
                  {copy["app.micTwoPage.tryAgainButton"]}
                </TryAgainButton>
              )}
              <ActionButton onClick={onSubmitTranscript}>
                {copy["app.micTwoPage.actionButtonSuccess"]}
              </ActionButton>
            </>
          );
        default:
          return null;
      }
    };

    const toggleTryAgainPopup = () => {
      setTryAgainPopup(!tryAgainOpen);
    };

    return (
      <motion.div
        initial={{ opacity: 0, x: 50 }}
        animate={{ opacity: 1, x: 0 }}
        exit={{ opacity: 0, x: -50 }}
      >
        <PageContent>
          {tryAgainOpen && (
            <TryAgainPopup
              attempt={task?.attempt || MAX_ATTEMPTS}
              onAccept={onTryAgain}
            />
          )}
          <Title>{copy["app.micTwoPage.title"]}</Title>

          {microphoneEnabled && renderProgress()}

          <Controls>{renderActions()}</Controls>
        </PageContent>
      </motion.div>
    );
  };

export default MicrophoneTwoPage;
