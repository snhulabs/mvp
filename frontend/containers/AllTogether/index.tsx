import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import PageContent from 'components/PageContent';
import Title from 'components/Title';
import TaskRecording from 'components/TaskRecording';
import TaskTranscript from 'components/TaskTranscript';
import { MAX_ATTEMPTS, PUT_ALL_TOGETHER_LENGTH } from 'config';
import PagePlanWrapper from 'components/Common/PagePlanWrapper';
import PageFirstPlan from 'components/Common/PageFirstPlan';
import PageSecondPlan from 'components/Common/PageSecondPlan';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';
import PageThirdPlan from 'components/Common/PageThirdPlan';
import SuccessScreen from 'components/SuccessScreen';
import useTask from 'hooks/useTask';
import SPEECH_CONTEXT from 'hooks/useTask/speechContext.const';
import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface AllTogetherPageProps {
  copy: Object;
}

const AllTogetherPage: React.FunctionComponent<AllTogetherPageProps> = ({ copy }) => {
  const { goTo } = React.useContext(NavigationContext);

  const {
    audioContext,
    inputSource,
    attempt,
    audioSrc,
    isProcessing,
    isRecording,
    onRetry,
    onSubmit,
    task,
    transcription,
    smileData,
    showSuccess,
    startRecording,
    stopRecording,
    canTryAgain,
    upload,
  } = useTask('PuttingItAllTogether');

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        <Title>{copy["app.allTogetherPage.title"]}</Title>

        <PagePlanWrapper>
          <PageFirstPlan visible={!transcription && !showSuccess}>
            <TaskRecording
              onStartRecording={startRecording}
              onStopRecording={stopRecording}
              isProcessing={isProcessing}
              isRecording={isRecording}
              timeout={PUT_ALL_TOGETHER_LENGTH}
              copy={copy["app.allTogetherPage.copy"]}
              key={`task-recording-${attempt}`}
              audioContext={audioContext}
              inputSource={inputSource}
            />
          </PageFirstPlan>

          <PageSecondPlan visible={!showSuccess && transcription}>
            <TaskTranscript
              attempt={attempt || MAX_ATTEMPTS}
              audioSrc={audioSrc}
              transcription={transcription}
              smileData={smileData}
              canTryAgain={canTryAgain}
              onSubmit={onSubmit}
              onRetry={onRetry}
              upload={upload}
              key={`task-transcript-${attempt}`}
              speechContext={SPEECH_CONTEXT.ALL_TOGETHER}
              isVisible={!showSuccess && transcription != null}
            />
          </PageSecondPlan>

          <PageThirdPlan visible={showSuccess && transcription}>
            <SuccessScreen
              copy={copy["app.allTogetherPage.successScreen"]}
              onNext={() => {
                goTo(ROUTING_STEPS.END);
              }}
            />
          </PageThirdPlan>
        </PagePlanWrapper>
      </PageContent>
    </motion.div>
  );
};

export default AllTogetherPage;
