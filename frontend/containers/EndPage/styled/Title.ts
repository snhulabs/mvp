import styled from 'styled-components';

import Wrapper from 'components/Title/styled/Wrapper';

export default styled(Wrapper)`
  margin-bottom: 20px;
  text-align: center;
`;
