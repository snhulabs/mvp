import React, { useEffect } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import PageContent from 'components/PageContent';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';

import Lines from './styled/Lines';
import Text from './styled/Text';
import Title from './styled/Title';

import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';
import parse from 'html-react-parser';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface EndPageProps {
  copy: Object;
}

const EndPage: React.FunctionComponent<EndPageProps> = ({ copy, ...restProps }) => {
  const { setupView } = React.useContext(NavigationContext);

  useEffect(() => {
    setupView(ROUTING_STEPS.END);
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <PageContent>
        <div />
        <div>
          <Lines />
          <Title>{copy["app.endPage.title"]}</Title>
          <Text>
            {parse(copy["app.endPage.text"])}
          </Text>
        </div>

        <div />
      </PageContent>
    </motion.div>
  );
};

export default EndPage;
