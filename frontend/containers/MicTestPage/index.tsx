/*

THIS IS OLD!!!!!
 */

import React, { useRef } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';
import Title from '../../components/Title';
import Link from '../../components/Link';

import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';
import parse from 'html-react-parser';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

//TODO: Needs to be refactor that he stream is shared with the main app
let audioStream = null;
if (process.browser) {
  const SAMPLE_RATE = 16000;
  const audioMedia = navigator.mediaDevices
    .getUserMedia({
      audio: {
        advanced: [
          {
            channelCount: 1,
            sampleRate: SAMPLE_RATE,
          },
        ],
      },
      video: false,
    })
    .then(stream => {
      console.log('Access granted');
      audioStream = stream;
      //Save the stream in the main app
    })
    .catch(err => {
      console.log(err.name);
      if (err.name === 'NotFoundError' || err.name === 'DevicesNotFoundError') {
        //required track is missing
      } else if (
        err.name === 'NotReadableError' ||
        err.name === 'TrackStartError'
      ) {
        //webcam or mic are already in use
      } else if (
        err.name === 'OverconstrainedError' ||
        err.name === 'ConstraintNotSatisfiedError'
      ) {
        //constraints can not be satisfied by avb. devices
      } else if (
        err.name === 'NotAllowedError' ||
        err.name === 'PermissionDeniedError'
      ) {
        //permission denied in browser
      } else if (err.name === 'TypeError' || err.name === 'TypeError') {
        //empty constraints object
      } else {
        //other errors
      }
    });
}

interface MicTestPageProps {
  copy: Object;
}

const MicTestPage: React.FunctionComponent<MicTestPageProps> = ({
  copy,
  ...restProps
}) => {
  const refPlayer = useRef(null);
  const refStartTest = useRef(null);
  let startTime = 0;
  let isStopped = false;

  const handleClick = e => {
    const options = { mimeType: 'audio/webm' };
    const recordedChunks = [];
    // @ts-ignore
    const mediaRecorder = new MediaRecorder(audioStream, options);

    isStopped = false;
    refStartTest.current.disabled = true;

    mediaRecorder.addEventListener('dataavailable', e => {
      if (e.data.size > 0) {
        recordedChunks.push(e.data);
      }
      const current = Date.now();
      const diff = current - startTime;
      if (diff > 3000 && !isStopped) {
        mediaRecorder.stop();
        isStopped = true;
      }
    });

    mediaRecorder.addEventListener('start', () => {
      startTime = Date.now();
    });

    mediaRecorder.addEventListener('stop', () => {
      refStartTest.current.disabled = false;
      const url = URL.createObjectURL(new Blob(recordedChunks));
      refPlayer.current.src = url;
      refPlayer.current.play();
    });

    mediaRecorder.start(3);
  };
  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <>
        <Title>{copy["app.micTestPage.title"]}</Title>
        <div>{copy["app.micTestPage.div"]}</div>
        <audio ref={refPlayer} controls></audio>
        <button onClick={handleClick} ref={refStartTest}>
          {copy["app.micTestPage.button"]}
        </button>
        <Link href="/page4">{copy["app.micTestPage.link"]}</Link>
      </>
    </motion.div>
  );
};

export default MicTestPage;
