import React, { useEffect, useState } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import { useLocalizedRouter } from 'hooks';

import Title from 'components/Title';
import MicSetupInstructions from 'components/MicSetupInstructions';
import MicSetupError from 'components/MicSetupError';
import ActionButton from 'components/ActionButton';

import PageContent from './styled/PageContent';
import Footer from './styled/Footer';
import { AudioContext } from 'context/audio.context';
import { AppUserContext } from 'context/appUser.context';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';
import { cmsApiClient } from 'utils/api/cms';
import { copyQuery } from 'utils/gql';

export const getStaticProps: GetStaticProps = async context =>
  Promise.all([
    cmsApiClient.query({ query: copyQuery() }),
  ]).then(([
      copyResults,
    ]) => ({
      props: {
        copy: copyResults.data.copies[0].copies,
      },
      revalidate: 10,
    })
  );

interface MicSetupPageProps {
  copy: Object;
}

const MicSetupPage: React.FunctionComponent<MicSetupPageProps> = ({
  copy,
  ...restProps
}) => {
  const { goTo, setupView } = React.useContext(NavigationContext);
  const { update } = React.useContext(AppUserContext);
  const { microphoneEnabled, permissionChecked } = React.useContext(
    AudioContext
  );
  const router = useLocalizedRouter();

  const onActionButtonClick = React.useCallback(() => {
    if (microphoneEnabled && permissionChecked) {
      update({ step: 3 }, () => {
        goTo(ROUTING_STEPS.MIC_ONE);
      });
    }
  }, [microphoneEnabled, permissionChecked]);

  React.useEffect(() => {
    setupView(ROUTING_STEPS.SETUP_MIC);
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <PageContent>
        <Title>{copy["app.setupPage.title"]}</Title>
        {!microphoneEnabled && permissionChecked ? (
          <MicSetupError copy={copy["app.setupPage.micSetupError"]} />
        ) : (
          <MicSetupInstructions onImageClick={onActionButtonClick} copy={copy["app.setupPage.micSetupInstructions"]} />
        )}
        <Footer>
          {microphoneEnabled && permissionChecked && (
            <ActionButton onClick={onActionButtonClick}>{copy["app.setupPage.actionButton"]}</ActionButton>
          )}
        </Footer>
      </PageContent>
    </motion.div>
  );
};

export default MicSetupPage;
