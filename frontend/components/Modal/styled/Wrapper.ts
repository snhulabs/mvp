import styled from 'styled-components';
import { motion } from 'framer-motion';

import { DEEP_LEVEL } from 'config';

export default styled(motion.div)`
  position: absolute;
  min-height: 30rem;
  height: 90vh;
  width: 90vw;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: ${DEEP_LEVEL[3]};
  background: ${({ theme }) => theme.colors.white};
  padding: 4rem 2rem;
`;
