import styled from 'styled-components';
import { rgba } from 'polished';
import { motion } from 'framer-motion';

import { DEEP_LEVEL } from 'config';

export default styled(motion.div)`
  position: fixed;
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: ${({ theme }) => rgba(theme.colors.black, 0.4)};
  z-index: ${DEEP_LEVEL[2]};
`;
