import styled from 'styled-components';

export default styled.div`
  font-size: 4vw;
  letter-spacing: 0.025em;
  position: relative;
  @media (min-height: 721px) {
  	padding-top: 14vh;
  }
  @media (max-height: 720px) {
  	padding-top: 9vh;
  	padding-bottom: 5vh;
  }
`;
