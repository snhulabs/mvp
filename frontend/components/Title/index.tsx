import React from 'react';

import Wrapper from './styled/Wrapper';

interface Props {
  children: any;
  className?: string;
}

export default function Title({ children, className }: Props) {
  return <Wrapper className={className}>{children}</Wrapper>;
}
