import React from 'react';

import Button from './styled/Button';

export default function ActionButton({
  children,
  onClick = () => {},
  className = '',
  disabled = false,
}) {
  const handleClickEvent = () => {
    if (!disabled) onClick();
  };

  return (
    <Button
      onClick={handleClickEvent}
      className={className}
      disabled={disabled}
    >
      {children}
    </Button>
  );
}
