import React from "react";
import Microphone from './styled/Microphone';
import VolumeMeter from "components/VolumeMeter";
import MicrophoneContainer from "./styled/MicrophoneContainer";
import { PROGRESS } from "utils/renderMicProgress";
import VolumeGraphContainer from "./styled/VolumeGraphContainer";

function VolumeGraph({
  audioContext,
  inputSource,
  progress,
  isCountdown,
  length
}) {
  return (
    <MicrophoneContainer>
      <Microphone
        isActive={progress === PROGRESS.MIC_RECORD}
        isChecked={progress === PROGRESS.SUCCESS}
        isCountdown={isCountdown}
        animateOutlines={false}
        time={length}>
      </Microphone>
      <VolumeGraphContainer>
        <VolumeMeter
          audioContext={audioContext}
          inputSource={inputSource}
          isActive={progress === PROGRESS.MIC_RECORD} />
      </VolumeGraphContainer>
    </MicrophoneContainer>
  );
}

export default VolumeGraph;
