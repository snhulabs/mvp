import styled from 'styled-components';

export default styled.div`
    position: relative;
    overflow: visible;
    width: 10vw;
    height: 10vw;
`;
