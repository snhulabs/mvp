import styled from 'styled-components';

import Lines from 'components/svg/Lines/Lines';

export default styled(Lines)`
  max-width: 365px;
  width: 100%;
`;
