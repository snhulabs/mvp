import styled from 'styled-components';

export default styled.p`
  margin-bottom: 40px;
  text-align: center;
`;
