import React from 'react';

import Paragraph from './styled/Paragraph';
import Wrapper from './styled/Wrapper';
import Lines from './styled/Lines';
import ActionButton from 'components/ActionButton';
import parse from 'html-react-parser';

interface SuccessScreenProps {
  copy: Object;
  message?: boolean;
  onNext(): void;
}

const SuccessScreen: React.FunctionComponent<SuccessScreenProps> = ({
  copy,
  message,
  onNext,
}) => (
  <Wrapper>
    <Lines isChecked />
    {message && (
      <Paragraph>
        {parse(copy["paragraph"])}
      </Paragraph>
    )}
    <ActionButton onClick={onNext}>{copy["actionButton"]}</ActionButton>
  </Wrapper>
);

export default SuccessScreen;
