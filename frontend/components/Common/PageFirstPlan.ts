import styled from 'styled-components';

import PagePlan from './PagePlan';

export default styled(PagePlan)`
  position: relative;
  z-index: 4;
`;
