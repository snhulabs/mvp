import styled from 'styled-components';

import PagePlan from './PagePlan';

export default styled(PagePlan)`
  left: 0;
  position: absolute;
  top: 0;
  z-index: 3;
`;
