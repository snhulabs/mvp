import styled from 'styled-components';

export default styled.div`
  align-items: center;
  display: ${({ visible }) => (visible ? 'flex' : 'none')};
  flex-direction: column;
  height: 100%;
  justify-content: center;
  visibility: ${({ visible }) => (visible ? 'visible' : 'hidden')};
  width: 100%;
`;
