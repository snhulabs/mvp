import styled from 'styled-components';

export default styled.div`
  height: 100%;
  position: relative;
  width: 100%;
  z-index: 1;
`;
