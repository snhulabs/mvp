import styled from 'styled-components';

export default styled.input`
  border: none;
  border-bottom: 1px solid black;
  border: 1px solid ${props => props.colour};
  outline: none;
  font-size: 1.6vw;
  margin: 1.3vw 0;
`;
