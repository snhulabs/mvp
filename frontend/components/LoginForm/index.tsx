import React, { useRef, useState } from 'react';
import { gql } from 'apollo-boost';

import Wrapper from './styled/Wrapper';
import FieldTitle from './styled/FieldTitle';
import Input from './styled/Input';
import ActionButton from 'components/ActionButton';
import { AppUserContext } from 'context/appUser.context';

export default function LoginForm({ onLoginCallback }) {
  const [inputColour, setColour] = useState('white');
  const { login } = React.useContext(AppUserContext);
  const [userId, setUserId] = useState('');
  const [isLoggingIn, setIsLoggingIn] = useState(false);

  const makeUserId = code => {
    return code.toUpperCase().slice(0, 20);
  };

  const checkCode = code => {
    // Performs validation checks for login.
    if (code.length > 20 || code.length < 12) {
      return 'ID must be at least 12 characters and no more than 20 characters.';
    };

    if (!code.match(/[A-Z]/gi) || !code.match(/[0-9]/gi)) {
      return 'At least 1 letter and 1 number must be provided.';
    };

    return true;
  };

  const updateVisualCheck = message => {
    if (checkCode(message)) {
      setColour('green');
    } else {
      setColour('red');
    }
  };

  const checkLogin = () => {
    const validated = checkCode(userId)
    if (validated && typeof(validated) == 'boolean') {
      login(userId, onLoginCallback);
      setIsLoggingIn(true);
    } else alert(validated);
  };


  return (
    <>
      <Wrapper>
        <FieldTitle>User ID</FieldTitle>
        <Input
          colour={inputColour}
          onChange={evt => {
            const { value, maxLength } = evt.target;
            const message = value.slice(0, maxLength);
            setUserId(makeUserId(message));
            updateVisualCheck(makeUserId(message));
          }}
          value={userId}
          type="text"
          maxLength="20"
          placeholder="Turk ID or Craigslist ID"
        />
      </Wrapper>
      <ActionButton disabled={isLoggingIn} onClick={checkLogin}>
        {isLoggingIn ? 'Logging in...' : 'Enter'}
      </ActionButton>
    </>
  );
}
