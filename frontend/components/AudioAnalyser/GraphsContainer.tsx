import styled from 'styled-components';

export default styled.div`
  width: 80%;
  margin-left: 10%;
  height: 15vh;
  padding-top: 1vh;
`;
