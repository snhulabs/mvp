import React from 'react';

import ArcGraph from 'components/ArcGraph';
import BarsGraph from 'components/BarsGraph';
import WaveSurferPlayer from 'components/WaveSurferPlayer';
import GraphsContainer from './GraphsContainer';

const animationDuration = 300;
const syllableRateRange = [50, 500]

var timeInterval = -1;
var audioDuration = 0;
var audioData = null;
var arcData = null;
var waveformObj = null;

export default function AudioAnalyser({ waveform, audioSrc, smileData, onReady, isVisible }) {
  const [waveFormLoaded, setWaveFormLoaded] = React.useState(false);
  const [dataIndex, setDataIndex] = React.useState(-1);

  React.useEffect(() => {
    if (smileData) {
      prepareAudioData(smileData)
    }
  }, [smileData]);

  const onAudioReady = (duration: number) => {
    audioDuration = duration;
    setWaveFormLoaded(true);

    onReady(duration);
  };

  const onAudioProcess = (currentTime: number) => {
    updateDataIndex(currentTime);
  };

  const onAudioSeek = (seekPercentage: number) => {
    const currentTime = seekPercentage * audioDuration;
    updateDataIndex(currentTime);
  };

  const updateDataIndex = (currentTime: number) => {
    const index = Math.floor(currentTime / timeInterval);
    if (index != dataIndex) {
      setDataIndex(index);
    }
  };

  const prepareAudioData = (data: any) => {
    if (!waveFormLoaded) return;

    const smileData = data.smileData;
    const samples = smileData.friendliness.length;
    timeInterval = audioDuration / samples;

    const properties = {
      friendliness: [],
      intelligence: [],
      likability: [],
      competence: [],
      syllable_rate: [],
    };

    const keys = Object.keys(properties);
    const finalData = [];

    keys.forEach(key => {
      if (key !== 'syllable_rate') {
        finalData.push({
          name: key,
          data: smileData[key],
        });
      }
    });

    audioData = finalData;
    arcData = smileData['syllable_rate']
      .map(x => clamp(normalize(x), 0, 1));

    onAudioSeek(0);
  };

  const normalize = (volume: number) => {
    return (volume - syllableRateRange[0]) / (syllableRateRange[1] - syllableRateRange[0])
  }

  const clamp = (value: number, min: number, max: number) => {
    return Math.min(Math.max(value, min), max);
  }

  const setWaveform = wave => {
    waveformObj = wave;

    if (waveform) {
      waveform(waveformObj);
    }
  };

  return (
    <div style={{width: '80%'}}>
      <WaveSurferPlayer
        waveformCallback={setWaveform}
        audioSrc={audioSrc}
        onAudioReady={onAudioReady}
        onAudioProcess={onAudioProcess}
        onAudioSeek={onAudioSeek}
        isVisible={isVisible}
      />

      <GraphsContainer>
        <BarsGraph
          data={audioData}
          dataIndex={dataIndex}
          animationDuration={animationDuration}
        />
        <ArcGraph
          data={arcData}
          dataIndex={dataIndex}
          animationDuration={animationDuration}
        />
      </GraphsContainer>
    </div>
  );
}
