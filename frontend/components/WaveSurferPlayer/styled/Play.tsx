import styled from 'styled-components';

import Play from 'components/svg/Play';

export default styled(Play)`
  width: 44px;
  height: 44px;
  margin-right: 20px;
  pointer-events: auto;
  cursor: pointer;
`;
