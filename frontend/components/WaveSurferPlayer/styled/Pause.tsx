import styled from 'styled-components';

import Pause from 'components/svg/Pause';

export default styled(Pause)`
  width: 44px;
  height: 44px;
  margin-right: 20px;
  pointer-events: auto;
  cursor: pointer;
`;
