import styled from 'styled-components';

import Download from 'components/svg/Download';

export default styled(Download)`
  width: 59px;
  height: 26px;
  margin-left: 20px;
  cursor: pointer;
  margin-left: 20px;
`;
