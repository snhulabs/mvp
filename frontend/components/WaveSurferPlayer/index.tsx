import React from 'react';

import Play from './styled/Play';
import Pause from './styled/Pause';
import Download from './styled/Download';
import WaveformContainer from './styled/WaveformContainer';
import Waveform from './styled/Waveform';

var waveform = null;

export default function WaveSurferPlayer({
  waveformCallback,
  audioSrc,
  onAudioReady,
  onAudioSeek,
  onAudioProcess,
  isVisible,
}) {
  const [isPlaying, setIsPlaying] = React.useState(false);

  const waveformRef = React.useRef(null);

  React.useEffect(() => {
    /*
     * NOTE: this is imported here because of this: https://github.com/katspaugh/wavesurfer.js/issues/2177
     * I tried to use dynamic import but that didn't work either.
     */
    const WaveSurfer = require('wavesurfer.js');

    // setup wavefrom
    waveform = WaveSurfer.create({
      container: waveformRef.current,
      waveColor: 'lightgrey',
      progressColor: '#F6BC19',
      cursorColor: '#755b10',
      barWidth: 3,
      barRadius: 3,
      cursorWidth: 1,
      barGap: 1,
      fillParent: true,
      responsive: true,
    });

    waveform.on('ready', () => {
      onAudioReady(waveform.getDuration());
    });

    waveform.on('seek', seekPercentage => {
      onAudioSeek(seekPercentage);
    });

    waveform.on('audioprocess', currentTime => {
      onAudioProcess(currentTime);
    });

    waveform.on('finish', () => {
      setIsPlaying(false);
    })

    loadAudio();

    waveformCallback(waveform);

    // return () => {
    //   if (waveform && waveform.current) {
    //     waveform.stop();
    //     waveformRef.current.destroy();
    //   }
    // }
  }, []);

  React.useEffect(() => {
    if (!isVisible && waveform) {
      waveform.pause();
      waveformCallback(null)
      waveform = null;
    }
  }, [isVisible])

  React.useEffect(() => {
    if (audioSrc && waveform) {
      loadAudio();
    }
  }, [audioSrc]);

  const loadAudio = () => {
    if (audioSrc) {
      waveform.load(audioSrc);
    }
  };

  const playPauseAudio = () => {
    if (waveform) waveform.playPause();

    setIsPlaying(waveform.isPlaying());
  };

  const downloadAudioFile = () => {
    fetch(audioSrc)
      .then(r => r.blob())
      .then(blob => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        a.download = 'recording.wav';
        document.body.appendChild(a);

        a.click();
        setTimeout(() => {
          document.body.removeChild(a);
          window.URL.revokeObjectURL(url);
        }, 100);
      });
  };

  const MediaControls = () => {
    if (isPlaying) {
      return (
        <a onClick={playPauseAudio}>
          <Pause />
        </a>
      );
    } else {
      return (
        <a onClick={playPauseAudio}>
          <Play />
        </a>
      );
    }
  };

  return (
    <WaveformContainer>
      <MediaControls />
      <Waveform ref={waveformRef} id="waveform"></Waveform>
      <a onClick={downloadAudioFile}>
        <Download />
      </a>
    </WaveformContainer>
  );
}
