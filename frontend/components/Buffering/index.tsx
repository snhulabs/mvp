import React from 'react';

import Wrapper from './styled/Wrapper';

interface BufferingProps {
  isLoading: boolean;
}

const Buffering: React.FunctionComponent<BufferingProps> = ({ isLoading }) => (
  <Wrapper isLoading={isLoading} />
);

export default Buffering;
