import styled, { keyframes, css } from 'styled-components';

const loadAnimation = keyframes`
  0%,
  80%,
  100% {
    box-shadow: 0 3.5em 0 -1.3em;
  }
  40% {
    box-shadow: 0 3.5em 0 0;
  }
`;

export default styled.div`
  position: relative;

  border-radius: 50%;
  width: 2.5em;
  height: 2.5em;
  animation-fill-mode: both;
  animation: ${loadAnimation} 1.8s infinite ease-in-out;

  color: ${({ theme }) => theme.colors.action};
  font-size: 10px;

  text-indent: -9999em;
  transform: translateZ(0);
  animation-delay: -0.16s;

  &:before,
  &:after {
    content: '';
    position: absolute;
    top: 0;

    border-radius: 50%;
    width: 2.5em;
    height: 2.5em;
    animation-fill-mode: both;
    animation: ${loadAnimation} 1.8s infinite ease-in-out;
  }

  &:before {
    left: -3.5em;
    animation-delay: -0.32s;
  }

  &:after {
    left: 3.5em;
  }

  ${({ isLoading }) =>
    !isLoading &&
    css`
      visibility: hidden;
      &:after,
      &:before {
        visibility: hidden;
      }
    `}
`;
