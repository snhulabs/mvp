import styled, { css } from 'styled-components';

export default styled.div`
  display: none;
  max-width: 70%;
  min-height: 5vw;
  padding-bottom: 15px;
`;
