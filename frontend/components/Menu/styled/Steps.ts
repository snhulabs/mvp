import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  margin: 10vh 0 0 5vw;
`;
