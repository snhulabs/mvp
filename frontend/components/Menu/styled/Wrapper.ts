import styled from 'styled-components';

export default styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  width: 15vw;
  min-height: 100vh;

  padding-top: 5vh;
  z-index: 999;
  background-color: ${({ theme }) => theme.colors.background};
`;
