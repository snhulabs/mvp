import styled from 'styled-components';
import Logo from 'components/Logo';

export default styled(Logo)`
  margin: 0 auto;
  width: 6vw;
  min-height: 60px;
  max-height: 100px;
`;
