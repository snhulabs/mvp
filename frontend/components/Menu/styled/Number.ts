import styled from 'styled-components';

export default styled.div`
  display: inline-flex;
  justify-content: center;
  align-items: center;
`;
