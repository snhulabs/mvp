import styled, { css } from 'styled-components';

export default styled.div`
  display: inline-flex;
  justify-content: center;
  align-items: center;

  width: 5vw;
  height: 5vw;
  margin-bottom: 1.2vw;

  background-color: rgba(255, 255, 255, 0.1);
  ${({ theme }) => css`
    color: ${theme.colors.white};
    font-family: ${theme.fonts.helvetica};
  `};

  ${({ visited, theme }) =>
    visited &&
    css`
      background-color: ${theme.colors.gray};
      color: ${theme.colors.black};
    `};

  ${({ active }) =>
    active &&
    css`
      width: 15vw;
      min-height: 10vh;
      height: auto;
      align-items: flex-start;
      justify-content: flex-start;

      div {
        &:first-child {
          width: 4vw;
          height: 5vw;
        }
        &:last-child {
          display: inline-flex;
          margin-top: calc(2.5vw - 0.55em); /* half height - line-height */
        }
      }
    `}
`;
