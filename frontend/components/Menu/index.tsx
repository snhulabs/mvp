import React from 'react';

import Wrapper from './styled/Wrapper';
import Logo from './styled/Logo';
import Steps from './styled/Steps';
import Step from './styled/Step';
import Number from './styled/Number';
import Title from './styled/Title';

import { NavigationContext } from 'context/navigation.context';

const TOTAL_STEPS = 6;

export default function Menu() {
  const { currentTitle, currentStep } = React.useContext(NavigationContext);
  const steps = [...Array(TOTAL_STEPS)];

  const renderSteps = () =>
    steps.map((_, index) => {
      const stepIndex = index + 1;
      const isActive = stepIndex === currentStep;
      const isTask = currentStep >= 4 && currentStep <= 6;
      const taskNumber = currentStep - 3;

      return (
        <Step
          key={`step${index + 1}`}
          visited={stepIndex <= currentStep}
          active={isActive}
        >
          <Number>{stepIndex}</Number>
          {isActive && (
            <Title>
              {currentTitle}
            </Title>
          )}
        </Step>
      );
    });

  return (
    <Wrapper>
      <Logo />
      <Steps>{renderSteps()}</Steps>
    </Wrapper>
  );
}
