import React from 'react';

import { MAX_ATTEMPTS } from 'config';

import Wrapper from './styled/Wrapper';
import Popup from './styled/Popup';
import ActionButton from './styled/Button';
import Title from './styled/Title';

interface Props {
  onAccept: () => void;
  onCancel?: () => void;
  attempt: number;
}

export default function TryAgainPopup({ onAccept, attempt }: Props) {
  const numberText = ['One', 'Two'];
  const remainingAttmepts = MAX_ATTEMPTS - attempt;

  return (
    <Wrapper>
      <Popup>
        <Title>
          {numberText[remainingAttmepts - 1]}
          {remainingAttmepts === 1 ? ' attempt ' : ' attempts '}
          remaining!
        </Title>
        <ActionButton onClick={onAccept}>Ok, let&apos;s try again</ActionButton>
      </Popup>
    </Wrapper>
  );
}
