import styled from 'styled-components';

export default styled.div`
  padding: 5vw;
  background-color: white;
  display: flex;
  flex-direction: column;

  transform: translateX(7.5vw);
`;
