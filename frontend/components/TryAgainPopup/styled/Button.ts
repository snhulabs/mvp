import styled from 'styled-components';
import ActionButton from 'components/ActionButton';

export default styled(ActionButton)`
  width: 25vw;
  margin-top: 5vw;
  font-size; 18px;
  font-weight: 400;
`;
