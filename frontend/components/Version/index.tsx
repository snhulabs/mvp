import React, { PureComponent } from 'react';

import { Host, Info, Button } from './styles';

const IS_LOCAL = process.env.ENV === 'local';

export default class Version extends PureComponent {
  state = {
    open: !IS_LOCAL,
  };

  toggle = () => this.setState({ open: !this.state.open });

  render() {
    const { open } = this.state;
    if (process.env.ENV === 'production') return null;

    return (
      <Host className="Version">
        {open && (
          <Info>
            {'  '}
            {IS_LOCAL ? 'local' : process.env.VERSION || 'undefined'}
          </Info>
        )}

        <Button onClick={this.toggle} open={open}>
          [ {open ? '-' : '+'} ]
        </Button>
      </Host>
    );
  }
}
