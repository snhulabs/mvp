import styled from 'styled-components';

const Host = styled.div`
  background-color: black;
  bottom: 50px;
  color: white;
  font-family: sans-serif;
  font-size: 11px;
  left: 20px;
  padding: 5px 10px;
  position: fixed;
  transition: right 0.5s;
  z-index: 50001;
`;

export default Host;
