import styled from 'styled-components';

const Info = styled.div`
  display: inline-block;
  margin: 0 10px 0 0;
`;

export default Info;
