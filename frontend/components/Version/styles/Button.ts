import styled from 'styled-components';

const Button = styled.button`
  cursor: pointer;
  display: inline-block;
  ${({ open }) => (open ? 'margin-left: 10px' : '')};
`;

export default Button;
