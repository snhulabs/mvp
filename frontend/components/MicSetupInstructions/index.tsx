import React from 'react';

import Description from 'components/Description';

import InstructionsWrapper from './styled/InstructionsWrapper';
import Info from './styled/Info';
import InfoGraphic from './styled/InfoGraphic';
import parse from 'html-react-parser';

export default function MicSetupInstructions({ onImageClick, copy }) {
  return (
    <InstructionsWrapper>
      <Description>
        {parse(copy["description"])}
      </Description>
      <Info>{copy["info"]}</Info>
      <InfoGraphic
        src="/assets/images/micSetupInfo.png"
        onClick={onImageClick}
      />
    </InstructionsWrapper>
  );
}
