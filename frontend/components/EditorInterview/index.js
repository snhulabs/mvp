import React from 'react';
import Wrapper from './styled/Wrapper';
import { useLocalizedRouter } from 'hooks';

export default function Editornterview({ text }) {
  const router = useLocalizedRouter();

  return (
    <Wrapper>
      <textarea value={text} cols="100" rows="20"></textarea>
    </Wrapper>
  );
}
