import styled, { css } from 'styled-components';

export default styled.button`
  display: flex;
  justify-content: center;
  width: 40%;
  padding: 1.8vmin 0;
  margin-top: 1vh;
  cursor: pointer;

  letter-spacing: 0.025em;
  font-family: Helvetica;
  font-weight: 300;
  font-size: 1.2vw;
  outline: none;

  ${({ theme }) => css`
    border: 2px solid ${theme.colors.gray};
    background-color: ${theme.colors.white};

    &:hover {
      border: 2px solid transparent;
      background-color: ${theme.colors.background};
      color: ${theme.colors.white};
    }
  `}

  ${({ selected, theme }) =>
    selected &&
    css`
      border: 2px solid transparent;
      background-color: ${theme.colors.background};
      color: ${theme.colors.white};
    `}
`;
