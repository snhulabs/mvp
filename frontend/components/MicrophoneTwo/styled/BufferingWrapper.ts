import styled from 'styled-components';

import { setBufferingWrapper } from 'utils/mixins';

export default styled.div`
  ${setBufferingWrapper()}
`;
