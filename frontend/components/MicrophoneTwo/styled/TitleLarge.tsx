import styled from 'styled-components';

export default styled.div`
  font-family: Palatino;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 36px;

  text-align: center;
  letter-spacing: 0.025em;
`;
