import styled from 'styled-components';

export default styled.div`
  font-size: 1.2vw;
  margin: 2vh 0;
  width: 70%;
  text-align: center;
`;
