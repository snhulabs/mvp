import styled from 'styled-components';

export default styled.div`
  margin: 2vh 0 4vh 0;
  font-size: 1vw;
  font-family: ${({ theme }) => theme.fonts.helvetica};
  text-align: center;
`;
