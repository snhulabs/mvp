import styled from 'styled-components';

export default styled.div`
  font-family: Palatino;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 36px;
  letter-spacing: 0.025em;

  margin-bottom: 30px;
  margin-left: 0;
  margin-right: 0;
  text-align: center;
`;
