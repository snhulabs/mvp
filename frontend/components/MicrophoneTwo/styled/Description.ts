import styled from 'styled-components';

export default styled.div`
  width: 70%;
  font-family: Palatino;
  font-size: 1.3vw;
  line-height: 24px;

  margin-bottom: 10px;
  margin-left: 0;
  margin-right: 0;
  text-align: center;
`;
