import * as React from 'react';
import { AnimatePresence } from 'framer-motion';

import { MIC_TWO_LENGTH } from 'config';
import Title from './styled/Title';
import Description from './styled/Description';
import BufferingWrapper from './styled/BufferingWrapper';
import useTimeout from 'hooks/useTimeout';
import useMicrophoneRecording from 'hooks/useMicrophoneRecording';
import PageSecondPlan from 'components/Common/PageSecondPlan';
import PagePlanWrapper from 'components/Common/PagePlanWrapper';
import PageFirstPlan from 'components/Common/PageFirstPlan';
import FadingScript from 'components/FadingScript';
import Buffering from 'components/Buffering';
import Player from 'components/Player';
import Info from './styled/Info';
import Link from './styled/Link';
import Troubleshooting from 'components/Troubleshooting';
import { PROGRESS } from 'utils/renderMicProgress';
import VolumeGraph from 'components/VolumeGraph';
import TitleLarge from './styled/TitleLarge';
import parse from 'html-react-parser';

export default function MicrophoneTwo({ copy, onUploadFinish, progress, isCountdown }) {
  const playerRef = React.useRef<HTMLAudioElement>(null);
  const [isOpenModal, setIsOpenModal] = React.useState<boolean>(false);
  const {
    audioContext,
    inputSource,
    audioURL,
    audioSrc,
    isProcessing,
    startRecording,
    stopRecording,
    clearRecording,
    uploadFinished,
    transcription,
    confidence,
    words,
    upload,
  } = useMicrophoneRecording();

  const { execute } = useTimeout({
    action: stopRecording,
    beforeAction: startRecording,
    timeout: MIC_TWO_LENGTH + 1000,
    condition: isCountdown,
    dependencies: [isCountdown]
  });

  const openModal = () => {
    setIsOpenModal(true);
  };

  React.useEffect(() => {
    playerRef.current.onloadedmetadata = () => {
      upload(playerRef.current.duration);
    };
  }, []);

  React.useEffect(() => {
    if (uploadFinished) {
      onUploadFinish(audioURL, transcription, confidence, words);
    }
  }, [uploadFinished, transcription, confidence, words]);

  React.useEffect(() => {
    switch (progress) {
      case PROGRESS.INTRO:
      case PROGRESS.CONFIRM_SUCCESS:
        clearRecording();
        break;
      case PROGRESS.MIC_RECORD:
        execute();
        break;
    }
  }, [progress])

  const renderVolumeGraph = () => {
    if (progress !== PROGRESS.SUCCESS) {
      return <VolumeGraph
        audioContext={audioContext}
        inputSource={inputSource}
        progress={progress}
        isCountdown={isCountdown}
        length={MIC_TWO_LENGTH} />
    }
    else {
      return null;
    }
  }

  const renderScript = () => {
    switch (progress) {
      case PROGRESS.MIC_RECORD:
        return <FadingScript time={MIC_TWO_LENGTH}>
          <div>{copy["fadingScriptFirst"]}</div>
          <div>{copy["fadingScriptSecond"]}</div>
          <div>{copy["fadingScriptThird"]}</div>
          <div>{copy["fadingScriptFourth"]}</div>
          <div>{copy["fadingScriptFifth"]}</div>
          <div>{copy["fadingScriptSixth"]}</div>
        </FadingScript>
      default:
        return null;
    }
  }

  return (
    <PagePlanWrapper>
      <PageFirstPlan visible={uploadFinished}>
        <TitleLarge>
          {parse(copy["titleLarge"])}
        </TitleLarge>
        <Info>
          <div>
            {copy["info"]}
          </div>
          <Link onClick={openModal}>
            {copy["link"]}
          </Link>
        </Info>
        <Player audioSrc={audioSrc} innerRef={playerRef} />
        <AnimatePresence exitBeforeEnter>
          {isOpenModal && <Troubleshooting setIsOpenModal={setIsOpenModal} />}
        </AnimatePresence>
      </PageFirstPlan>
      <PageSecondPlan visible={!uploadFinished}>
        <Title>{parse(copy["title"])}</Title>
        <Description>
          {parse(copy["descriptionTop"])}
        </Description>
        <Description>
          {parse(copy["descriptionBottom"])}
        </Description>

        {renderScript()}
        {renderVolumeGraph()}

        <BufferingWrapper>
          <Buffering isLoading={isProcessing} />
        </BufferingWrapper>
      </PageSecondPlan>
    </PagePlanWrapper>
  );
}
