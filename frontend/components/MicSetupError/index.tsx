import React from 'react';

import { AnimatePresence } from 'framer-motion';

import WarningIcon from 'components/svg/Warning';

import Copy from './styled/Copy';
import Wrapper from './styled/Wrapper';
import MicErrorBackground from './styled/MicErrorBackground';
import InstructionWrapper from './styled/InstructionWrapper';
import Info from './styled/Info';
import Link from './styled/Link';
import Title from './styled/Title';
import BrowserImage from './styled/BrowserImage';
import ActionButton from 'components/ActionButton';
import Troubleshooting from 'components/Troubleshooting';

interface MicSetupErrorProps {
  copy: Object;
}

export default function MicSetupError({ copy }: MicSetupErrorProps) {
  const [isOpenModal, setIsOpenModal] = React.useState<boolean>(false);
  // TODO Move to utils
  const reloadPage = () => {
    window.location.href = `${window.location.origin}/en-us/login`;
  };

  const openModal = () => {
    setIsOpenModal(true);
  };

  return (
    <Wrapper>
      <MicErrorBackground isCrossed />
      <InstructionWrapper>
        <Title>{copy["title"]}</Title>
        <WarningIcon />
        <Copy>{copy["copy"]}</Copy>
        <Copy>
          {copy["secondCopy"]}
        </Copy>
        <BrowserImage src="/assets/images/micPermission.png" />
        <Copy>
          {copy["thirdCopy"]}
        </Copy>
        <Info>
          <div>{copy["info"]}</div>
          <Link onClick={openModal}>
            {copy["link"]}
          </Link>
          <AnimatePresence exitBeforeEnter>
            {isOpenModal && <Troubleshooting setIsOpenModal={setIsOpenModal} />}
          </AnimatePresence>
        </Info>
        <ActionButton onClick={reloadPage}>{copy["actionButton"]}</ActionButton>
      </InstructionWrapper>
    </Wrapper>
  );
}
