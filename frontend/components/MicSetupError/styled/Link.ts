import styled from 'styled-components';

export default styled.a`
  display: inline-block;

  color: ${({ theme }) => theme.colors.black};
  text-decoration: none;
  font-weight: 600;
  margin: 1vh 0;
`;
