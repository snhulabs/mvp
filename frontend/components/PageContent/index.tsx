import React from 'react';

import Menu from 'components/Menu';

import Wrapper from './styled/Wrapper';
import PageContentWrapper from './styled/PageContentWrapper';

interface Props {
  children: any;
  className?: string;
}

export default function PageContent({ children, className }: Props) {
  return (
    <Wrapper>
      <Menu />
      <PageContentWrapper className={className}>{children}</PageContentWrapper>
    </Wrapper>
  );
}
