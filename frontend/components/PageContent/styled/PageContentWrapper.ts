import styled from 'styled-components';

export default styled.div`
  flex: 1;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  max-height: 100vh;
  overflow-y: auto;
`;
