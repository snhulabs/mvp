import styled from 'styled-components';

export default styled.div`
  display: flex;
  position: relative;

  width: 100vw;
  min-height: 100vh;
  background-color: ${({ theme }) => theme.colors.white};
`;
