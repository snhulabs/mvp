import React, { useRef, useState } from 'react';

import Wrapper from './styled/Wrapper';
import Title from './styled/Title';
import Description from './styled/Description';
import parse from 'html-react-parser';

export default function IntroMic({
  copy,
  secondary = false,
}: {
  copy: Object;
  secondary?: boolean;
}) {
  return (
    <Wrapper>
      <Title>
        {secondary ? (
          copy["secondaryTitle"]
        ) : (
          <>
            {parse(copy["title"])}
          </>
        )}
      </Title>
      <Description>
        {secondary ? (
          <>
            {parse(copy["secondaryDescription"])}
          </>
        ) : (
          <>
            {parse(copy["description"])}
          </>
        )}
      </Description>
    </Wrapper>
  );
}
