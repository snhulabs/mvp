import styled from 'styled-components';

export default styled.div`
  position: relative;
  width: 40%;
  max-height: 70px;
  height: 10vh;
  background-color: ${({ theme }) => theme.colors.gray};
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 7vh;
`;
