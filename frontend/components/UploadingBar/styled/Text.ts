import styled from 'styled-components';

import { DEEP_LEVEL } from 'config';

export default styled.div`
  position: absolute;
  width: 100%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: ${DEEP_LEVEL[2]};

  text-align: center;
  letter-spacing: 0.025em;
  font-family: Helvetica;
  font-size: 1.4vw;
`;
