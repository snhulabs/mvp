import styled from 'styled-components';

import { DEEP_LEVEL } from 'config';

export default styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;

  transform-origin: left center;
  transform: scaleX(0);
  background-color: ${({ theme }) => theme.colors.action};
  z-index: ${DEEP_LEVEL[1]};
`;
