import React, { useEffect, useRef } from 'react';

import Bar from './styled/Bar';
import Progress from './styled/Progress';
import Text from './styled/Text';

interface UploadingBarProps {
  setUploaded: (uploaded) => void;
}

const UploadingBar = ({ setUploaded }: UploadingBarProps): JSX.Element => {
  const progressRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    move();
  }, []);

  // fake loader
  const move = () => {
    let width = 0;
    const id = setInterval(() => frame(), 20);
    const frame = () => {
      if (width >= 100) {
        clearInterval(id);
        setTimeout(() => {
          setUploaded(true);
        }, 1000);
      } else {
        width++;
        progressRef.current.style.transform = 'scaleX(' + width / 100 + ')';
      }
    };
  };

  return (
    <Bar>
      <Text>Please wait for the upload to finish</Text>
      <Progress ref={progressRef} />
    </Bar>
  );
};

export default UploadingBar;
