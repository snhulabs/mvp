import styled from 'styled-components';

export default styled.div`
    position: relative;
    overflow: visible;
    width: 50%;
    display: inline-block;
    height: 80%;
`;
