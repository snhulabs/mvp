import React from 'react';
import * as d3 from 'd3';
import GraphContainer from './styled/GraphContainer';

const width = 400;
const height = 100;
const yAxisOffset = 80;
const xGraphOffset = 100;
const yellowColour = '#F6BC19';
const circleRadius = 5;
const lineWidth = 2;
const formatPercent = d3.format('.0%');
const yAxisNamesFormat = (name) => {
  return name.charAt(0).toUpperCase() + name.slice(1)
}

function BarsGraph({ data, dataIndex, animationDuration }) {
  const svgRef = React.useRef();
  const xScale = React.useRef(null);
  const yScale = React.useRef(null);

  React.useEffect(() => {
    if (data == null) return;

    const svg = d3
      .select(svgRef.current)
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr('viewBox', `0 0 ${width} ${height}`)
      // .attr('width', width)
      // .attr('height', height);

    xScale.current = d3.scaleLinear().domain([0, 1]).range([xGraphOffset, width]);

    const xAxis = d3
      .axisBottom(xScale.current)
      .tickValues([0, 1])
      .tickFormat(formatPercent);

    yScale.current = d3
      .scaleBand()
      .domain(data.map(d => d.name))
      .range([0, height])

    const yAxis = d3
      .axisLeft(yScale.current)
      .tickFormat(yAxisNamesFormat);

    svg
      .select('.x-axis')
      .style('transform', `translateY(-25px)`)
      .call(xAxis)
      .select('.domain')
      .remove();

    svg
      .select('.y-axis')
      .style('transform', `translateX(${yAxisOffset}px)`)
      .attr('class', 'bar-graph-labels')
      .call(yAxis)
      .select('.domain')
      .remove();

    // add the Y gridlines
    const gridScale = d3.scaleLinear()
      .domain([0, 1])
      .range([xGraphOffset, width]);

    svg
      .select('.grid-lines')
      .attr('class', 'bar-graph-tick')
      .call(
        d3.axisBottom(gridScale)
          .ticks(5)
          .tickSize(height)
          .tickFormat("")
      )
      .select('.domain')
      .remove();

    svg
      .select('.bar-graph-caption')
      .style('transform', `translate(${width / 2}px, ${height + 10}px)`)
      .style('text-anchor', 'middle');

    if (data == null || xScale.current == null || yScale.current == null) return;
    if (dataIndex < 0 || dataIndex >= data[Object.keys(data)[0]].length) return;

    // const svg = d3.select(svgRef.current)
    var lines = svg.selectAll('.line').data(data);

    lines
      .enter()
      .append('line')
      .merge(lines)
      .transition()
      .ease(d3.easeQuad)
      .duration(animationDuration)
      .attr('class', 'line')
      .attr('x1', xScale.current(0))
      .attr('x2', value => xScale.current(value.data[dataIndex]))
      .attr('y1', value => yScale.current(value.name) + yScale.current.bandwidth() / 2)
      .attr('y2', value => yScale.current(value.name) + yScale.current.bandwidth() / 2)
      .attr('stroke', yellowColour)
      .attr('stroke-width', lineWidth);

    var circles = svg.selectAll('.circle').data(data);

    circles
      .enter()
      .append('circle')
      .merge(circles)
      .transition()
      .ease(d3.easeQuad)
      .duration(animationDuration)
      .attr('class', 'circle')
      .attr('cx', value => xScale.current(value.data[dataIndex]))
      .attr('cy', value => yScale.current(value.name) + yScale.current.bandwidth() / 2)
      .attr('r', circleRadius)
      .attr('fill', yellowColour);
  }, [dataIndex]);

  return (
    <GraphContainer>
      <svg ref={svgRef} className="bar-graph">
        <g className="x-axis" />
        <g className="y-axis" />
        <g className="grid-lines" />
        <text className="bar-graph-caption">Listener Interpretation</text>
      </svg>
    </GraphContainer>
  );
}

export default BarsGraph;
