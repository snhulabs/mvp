import React from 'react';
import { withRouter } from 'next/router';
import { WithRouterProps } from 'next/dist/client/with-router';
import NextLink, { LinkProps as NextLinkProps } from 'next/link';

import { isRemotePath, join, trimTrailingSlash } from 'utils/path';

interface LinkProps extends NextLinkProps, WithRouterProps {
  children: React.ReactNode | React.ReactNode[];
  disableI18n?: boolean;
}

const Link = ({
  children,
  router,
  href,
  as,
  disableI18n = !!process.env.DISABLE_I18N,
  ...restProps
}: LinkProps) => {
  const normalizedHref = {
    pathname: trimTrailingSlash(
      typeof href === 'string' ? href : href.pathname
    ),
    query: typeof href === 'string' || !href.query ? {} : href.query,
    hash: typeof href === 'string' || !href.hash ? '' : href.hash,
  };

  if (typeof normalizedHref.query === 'string') {
    throw new Error(
      'query values of string type are not supported. Use object notation instead.'
    );
  }

  const normalizedAs = trimTrailingSlash(
    as ? as.toString() : normalizedHref.pathname
  );

  const localizedHref = {
    pathname: normalizedHref.pathname,
    query: { ...normalizedHref.query },
    hash: normalizedHref.hash,
  };

  let localizedAs = normalizedAs;

  if (!isRemotePath(href.toString()) && !disableI18n) {
    const lang = router.query.lang || process.env.DEFAULT_LOCALE;

    if (!lang) {
      throw new Error('lang not specified in router query');
    }

    localizedHref.query.lang = lang;

    // Localize internal paths
    const pathnameParts = ['/[lang]'];
    const asParts = [`/${lang}`];

    if (
      !(typeof href === 'string' ? href : href.pathname)
        .toString()
        .startsWith('/')
    ) {
      // Relative path, so prefix with current pathname
      pathnameParts.push(router.pathname);
      asParts.push(router.pathname);
    }

    // Push actual href now
    pathnameParts.push(normalizedHref.pathname);
    asParts.push(normalizedAs);

    // Override values with localized ones
    localizedHref.pathname = join(pathnameParts);
    localizedAs = join(asParts);
  }

  localizedAs += localizedHref.hash;

  return (
    <NextLink
      href={localizedHref}
      as={localizedAs}
      {...restProps}
      passHref={true}
    >
      {children}
    </NextLink>
  );
};

Link.displayName = 'Link';

export default withRouter(React.memo(Link));
