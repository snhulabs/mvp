import React from 'react';

import FadingScript from 'components/FadingScript';
import Buffering from 'components/Buffering';
import { MIC_ONE_LENGTH } from 'config';
import Title from './styled/Title';
import Description from './styled/Description';
import BufferingWrapper from './styled/BufferingWrapper';
import Player from 'components/Player';
import PagePlanWrapper from 'components/Common/PagePlanWrapper';
import PageFirstPlan from 'components/Common/PageFirstPlan';
import PageSecondPlan from 'components/Common/PageSecondPlan';
import useTimeout from 'hooks/useTimeout';
import useMicrophoneRecording from 'hooks/useMicrophoneRecording';
import parse from 'html-react-parser';

export default function MicrophoneOne({ onUploadFinish, copy }) {
  const playerRef = React.useRef<HTMLAudioElement>(null);
  const {
    audioURL,
    audioSrc,
    isProcessing,
    startRecording,
    stopRecording,
    uploadFinished,
    transcription,
    confidence,
    words,
    upload,
  } = useMicrophoneRecording();

  useTimeout({
    action: stopRecording,
    beforeAction: startRecording,
    timeout: MIC_ONE_LENGTH + 1000,
  });

  React.useEffect(() => {
    playerRef.current.onloadedmetadata = () => {
      upload(playerRef.current.duration);
    };
  }, []);

  React.useEffect(() => {
    if (uploadFinished) {
      onUploadFinish(audioURL, transcription, confidence, words);
    }
  }, [uploadFinished, transcription, confidence, words]);

  return (
    <PagePlanWrapper>
      <PageFirstPlan visible={false}>
        <Player audioSrc={audioSrc} innerRef={playerRef} />
      </PageFirstPlan>
      <PageSecondPlan visible={true}>
        <Title>
          {parse(copy["title"])}
        </Title>
        <Description>
          {parse(copy["description"])}
        </Description>
        <FadingScript time={MIC_ONE_LENGTH}>
          <div>{copy["fadingScriptTop"]}</div>
          <div>{parse(copy["fadingScriptBottom"])}</div>
        </FadingScript>
        <BufferingWrapper>
          <Buffering isLoading={isProcessing} />
        </BufferingWrapper>
      </PageSecondPlan>
    </PagePlanWrapper>
  );
}
