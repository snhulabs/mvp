import styled from 'styled-components';

export default styled.div`
  font-size: 14px;
  line-height: 24px;
  font-family: Helvetica;
  font-weight: 300;
  margin: 30px 0;
  text-align: center;
`;
