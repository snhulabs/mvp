import styled from 'styled-components';

export default styled.div`
  font-size: 24px;
  font-weight: 400;
  font-family: Palatino;
  line-height: 36px;
  letter-spacing: 1px;
  text-align: center;
`;
