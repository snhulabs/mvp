import styled from 'styled-components';

export default styled.span`
  color: ${({ theme }) => theme.colors.black};
  font-family: Helvetica;
  font-size: 14px;
  font-weight: 300;
  letter-spacing: 1px;
  opacity: 0.5;
  position: absolute;
  right: 40px;
  top: 30px;
  z-index: 5;
`;
