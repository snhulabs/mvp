import { AppUserContext } from 'context/appUser.context';
import * as React from 'react';

import Container from './styled/Container';

export default function UserIdentifierLabel(): JSX.Element {
  const { userId } = React.useContext(AppUserContext);

  if (!userId) return null;

  return <Container>User ID {userId}</Container>;
}
