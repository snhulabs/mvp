import React from 'react';
import * as d3 from 'd3';
import GraphContainer from 'components/BarsGraph/styled/GraphContainer';

const width = 400;
const height = 100;
const yellowColour = '#F6BC19';
const textYOffset = 10;
const textXOffset = 50;

const minArc = -0.5 * Math.PI;
const maxArc = 0.5 * Math.PI

const solidArcOuterRadius = 120;
const solidArcRadiusOffset = 30;

const lineArcRadiusOffset = 2;
const lineArcOuterRadius =
  solidArcOuterRadius -
  solidArcRadiusOffset +
  solidArcRadiusOffset / 2 +
  lineArcRadiusOffset / 2;

const solidArcGenerator = d3
  .arc()
  .innerRadius(solidArcOuterRadius - solidArcRadiusOffset)
  .outerRadius(solidArcOuterRadius);

function ArcGraph({ data, dataIndex, animationDuration }) {
  const svgRef = React.useRef();
  const arcPath = React.useRef(null);
  const previousValue = React.useRef(-0.5 * Math.PI);

  React.useEffect(() => {
    const svg = d3
      .select(svgRef.current)
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr('viewBox', `0 0 ${width} ${height}`)
      // .attr('width', width)
      // .attr('height', height);

    const lineArcGenerator = d3
      .arc()
      .innerRadius(lineArcOuterRadius - lineArcRadiusOffset)
      .outerRadius(lineArcOuterRadius)
      .startAngle(-0.5 * Math.PI)
      .endAngle(0.5 * Math.PI);

    const translateString = `translate(${width / 2}px, ${height}px)`;
    d3.select('.line-arc')
      .append('path')
      .attr('d', lineArcGenerator)
      .attr('fill', yellowColour)
      .style('transform', translateString);

    solidArcGenerator.startAngle(-0.5 * Math.PI).endAngle(-0.5 * Math.PI);

    arcPath.current = svg
      .select('.solid-arc')
      .append('path')
      .style('transform', translateString)
      .attr('fill', yellowColour)
      .attr('d', solidArcGenerator);

    svg
      .select('.left-text')
      .style(
        'transform',
        `translate(${textXOffset}px, ${height - textYOffset}px)`
      )
      .style('text-anchor', 'middle');

    svg
      .select('.right-text')
      .style(
        'transform',
        `translate(${width - textXOffset}px, ${height - textYOffset}px)`
      )
      .style('text-anchor', 'middle');

    svg
      .select('.caption')
      .style('transform', `translate(${width / 2}px, ${height + 10}px)`)
      .style('text-anchor', 'middle');
  }, []);

  React.useEffect(() => {
    if (arcPath.current == null || data == null) return;
    if (dataIndex < 0 || dataIndex >= data[Object.keys(data)[0]].length) return;

    arcPath.current
      .transition()
      .ease(d3.easeQuad)
      .duration(animationDuration)
      .attrTween('d', arcTween(data[dataIndex]));
  }, [dataIndex]);

  const arcTween = (value: number) => {
    return function () {
      const newValue = clamp((value - 0.5) * Math.PI, minArc, maxArc);
      var interpolate = d3.interpolate(previousValue.current, newValue);

      previousValue.current = newValue;

      return function (t: number) {
        solidArcGenerator.endAngle(interpolate(t));
        return solidArcGenerator();
      };
    };
  };

  const clamp = (value: number, min: number, max: number) => {
    return Math.min(Math.max(value, min), max);
  }

  return (
    <GraphContainer>
      <svg ref={svgRef} className="arc-graph">
        <g className="line-arc"></g>
        <g className="solid-arc"></g>
        <text className="left-text arc-graph-labels">Slow</text>
        <text className="right-text arc-graph-labels">Fast</text>
        <text className="caption arc-text">Words per Minute</text>
      </svg>
    </GraphContainer>
  );
}

export default ArcGraph;
