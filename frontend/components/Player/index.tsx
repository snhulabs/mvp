import * as React from 'react';
import { CSSProperties } from 'react';
import ReactAudioPlayer from 'react-audio-player';

import Buffering from 'components/Buffering';
import Audio from './styled/Audio';
import Container from './styled/Container';
import AudioWrapper from './styled/AudioWrapper';
import Optional from './styled/Optional';
import Download from './styled/Download';

const playerStyles: CSSProperties = {
  outline: 'none',
  boxShadow: 'none',
};

export default function Player({ audioSrc, innerRef }): JSX.Element {
  return (
    <Container>
      <AudioWrapper isLoading={false}>
        <ReactAudioPlayer
          src={audioSrc}
          controls={true}
          controlsList="nodownload"
          className="audioPlayer"
          style={playerStyles}
        />
        <Audio src={audioSrc} ref={innerRef} />
        {audioSrc && (
          <Download
            href={audioSrc}
            download="output.wav"
            title="Download recorded audio"
          >
            Download audio
            <Optional>(Optional)</Optional>
          </Download>
        )}
      </AudioWrapper>
      <Buffering isLoading={false} />
    </Container>
  );
}
