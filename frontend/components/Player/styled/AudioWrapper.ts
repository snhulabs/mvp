import styled, { css } from 'styled-components';

export default styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;

  ${({ isLoading }) =>
    isLoading &&
    css`
      visibility: hidden;
    `}
`;
