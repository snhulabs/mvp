import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: -2px;
  left: 0;
  background-color: ${({ theme }) => theme.colors.action};
  height: 10px;
  width: 10px;
  border-radius: 50%;

  transform: translateX(${({ progress }) => (268 * progress) / 100}px);
  transition: transform 0.3s linear;
`;
