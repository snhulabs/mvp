import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: ${({ progress }) => progress}%;
  transition: width 0.3s linear;
  background-color: ${({ theme }) => theme.colors.action};
`;
