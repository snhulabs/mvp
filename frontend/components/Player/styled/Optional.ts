import styled from 'styled-components';

export default styled.span`
  font-size: 10px;
  margin-left: 5px;
`;
