import styled from 'styled-components';

export default styled.button`
  background-color: transparent;
  border: 0;
  box-shadow: none;
  cursor: pointer;
  margin: 0 0 25px 5px;
  outline: 0;
  padding: 0;
  width: 5rem;

  &:focus,
  &:active {
    outline: 0;
  }
`;
