import styled from 'styled-components';

export default styled.a`
  color: ${({ theme }) => theme.colors.action};
  font-size: 12px;
  margin-top: 20px;
`;
