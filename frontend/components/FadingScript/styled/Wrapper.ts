import styled from 'styled-components';

export default styled.div`
  position: relative;
  overflow: hidden;
  width: 60vw;
  height: 15vw;
  font-size: 3vw;

  mask-image: linear-gradient(
    0deg,
    rgba(0, 0, 0, 0) 0%,
    rgba(0, 0, 0, 1) 50%,
    rgba(0, 0, 0, 0) 100%
  );
`;
