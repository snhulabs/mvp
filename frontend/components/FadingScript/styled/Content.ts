import styled, { keyframes } from 'styled-components';

const animate = keyframes`
    0% {
        transform: translate(-50%, 100%)
    }

    100% {
        transform: translate(-50%, -15vw);
    }
`;

export default styled.div`
  position: absolute;
  left: 50%;
  bottom: 0;
  animation: ${animate} ${({ time }) => `${time}ms`} linear forwards;

  div {
    font-size: 2.5rem;
    text-align: center;
  }
`;
