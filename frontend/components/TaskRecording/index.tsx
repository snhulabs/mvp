import React, { useRef } from 'react';

import ActionButton from 'components/ActionButton';
import Buffering from 'components/Buffering';

import Wrapper from './styled/Wrapper';
import ButtonsWrapper from './styled/ButtonsWrapper';
import TaskDescription from './styled/TaskDescription';
import Steps from './styled/Steps';
import Step from './styled/Step';
import StepTitle from './styled/StepTitle';
import Arrow from './styled/Arrow';
import StepDetails from './styled/StepDetails';
import Microphone from './styled/Microphone';
import FinishButton from './styled/FinishButton';
import BufferingWrapper from './styled/BufferingWrapper';
import BufferingText from './styled/BufferingText';
import useTimeout from 'hooks/useTimeout';
import VolumeGraph from 'components/VolumeGraph';
import { PROGRESS } from 'utils/renderMicProgress';

export default function TaskRecording({
  copy,
  isProcessing,
  isRecording,
  onStartRecording,
  onStopRecording,
  timeout,
  audioContext,
  inputSource,
}) {
  const [triggerTimeout, setTriggerTimeout] = React.useState<boolean>(false);
  const [progress, setProgress] = React.useState(PROGRESS.INTRO);
  const { taskDescription, steps } = copy;

  const recordingStopTime = useRef(0);
  const intervalHandler = useRef(null);

  const onStart = () => {
    setTriggerTimeout(true);
    setProgress(PROGRESS.MIC_RECORD);

    intervalHandler.current = setInterval(
      () => (recordingStopTime.current = recordingStopTime.current + 0.01),
      10
    );
  };

  const renderSteps = () =>
    steps.map(({ title, details }, idx) => (
      <Step key={`step${idx}`}>
        <StepTitle>
          <div>{title.toUpperCase()}</div>
          <Arrow hasNext />
        </StepTitle>
        <StepDetails>{details}</StepDetails>
      </Step>
    ));

  const onStop = () => {
    onStopRecording();
    clearInterval(intervalHandler.current);
    setTriggerTimeout(false);
    setProgress(PROGRESS.INTRO);
  };

  const { clear } = useTimeout({
    action: onStop,
    beforeAction: onStartRecording,
    condition: triggerTimeout,
    dependencies: [triggerTimeout],
    timeout: timeout + 1000,
  });

  const onClickStop = () => {
    onStop();
    clear();
  };

  React.useEffect(() => {
    return () => {
      recordingStopTime.current = 0;
    };
  }, []);

  return (
    <>
      <Wrapper>
        <TaskDescription
          dangerouslySetInnerHTML={{ __html: taskDescription }}
        />
        <Steps>{renderSteps()}</Steps>

        <VolumeGraph
          audioContext={audioContext}
          inputSource={inputSource}
          progress={progress}
          isCountdown={triggerTimeout}
          length={timeout}
        />

      </Wrapper>
      <ButtonsWrapper>
        {isRecording && (
          <FinishButton onClick={onClickStop}>{copy["finishButton"]}</FinishButton>
        )}
        {!isRecording && isProcessing && (
          <>
            <BufferingWrapper>
              <Buffering isLoading />
            </BufferingWrapper>
            <BufferingText>{copy["bufferingText"]}</BufferingText>
          </>
        )}
        {!isRecording && !isProcessing && (
          <ActionButton onClick={onStart}>{copy["actionButton"]}</ActionButton>
        )}
      </ButtonsWrapper>
    </>
  );
}
