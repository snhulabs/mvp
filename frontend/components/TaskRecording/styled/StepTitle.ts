import styled, { css } from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;

  ${({ theme }) => css`
    color: ${theme.colors.action};
  `}
`;
