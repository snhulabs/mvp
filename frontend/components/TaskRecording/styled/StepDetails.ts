import styled from 'styled-components';

export default styled.div`
  color: ${({ theme }) => theme.colors.grayDark};
  max-width: 160px;
  margin-top: 15px;
  font-size: 0.9vw;
  display: flex;
  align-items: center;
  text-align: left;
  width: 100%;
`;
