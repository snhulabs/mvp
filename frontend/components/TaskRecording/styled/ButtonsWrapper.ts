import styled from 'styled-components';

export default styled.div`
  position: relative;
  z-index: 9999;
  width: 100%;
`;
