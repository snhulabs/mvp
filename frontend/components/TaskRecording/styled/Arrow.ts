import styled, { css } from 'styled-components';

export default styled.div`
  position: relative;
  margin-top: 1vh;

  ${({ theme }) => css`
    border: 1px solid ${theme.colors.action};
  `}

  ${({ theme, hasNext }) =>
    hasNext &&
    css`
      &::after {
        content: ' ';
        width: 1vw;
        height: 1vw;
        right: 0;
        position: absolute;
        border-top: 2px solid ${theme.colors.action};
        border-right: 2px solid ${theme.colors.action};
        transform: translate(2px) rotate(45deg);
        transform-origin: top right;
      }
    `}
`;
