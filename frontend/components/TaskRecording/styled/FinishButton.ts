import styled from 'styled-components';
import ActionButton from 'components/ActionButton';

export default styled(ActionButton)`
  background-color: ${({ theme }) => theme.colors.gray};
`;
