import styled from 'styled-components';
import Microphone from 'components/svg/Microphone';

export default styled(Microphone)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
