import styled from 'styled-components';

export default styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  width: 100%;
  margin: 4vh 0;
`;
