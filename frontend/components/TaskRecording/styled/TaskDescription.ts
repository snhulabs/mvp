import styled from 'styled-components';

export default styled.div`
  text-align: center;
  width: 60%;
  line-height: 30px;
  font-size: 1.3vw;
`;
