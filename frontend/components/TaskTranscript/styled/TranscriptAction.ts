import styled from 'styled-components';

export default styled.button`
  background-color: ${({ theme }) => theme.colors.action};
  border: 0;
  border-radius: 5px;
  color: ${({ theme }) => theme.colors.black};
  cursor: pointer;
  font-size: 12px;
  outline: none;
  line-height: 14px;
  opacity: 0.6;
  padding: 7px 12px 5px;
  position: absolute;
  right: 10px;
  top: 10px;
  transition: opacity 0.3s linear;
  z-index: 1;

  &:hover {
    opacity: 1;
  }
`;
