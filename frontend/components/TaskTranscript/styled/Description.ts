import styled from 'styled-components';

export default styled.p`
  font-size: 1.1vw;
  width: 90%;
  line-height: calc(2vw);
  // font-family: Helvetica;
  font-weight: 400;
  letter-spacing: 0.05em;
  // padding-bottom: 20px;
  padding-top: 20px;
  padding-left: 20px;
  padding-right: 20px;
  text-align: center;
  margin-bottom: 0px;
`;
