import styled from 'styled-components';

export default styled.div`
  background-color: #f3f5f8;
  cursor: ${({ hasAction }) => (hasAction ? 'pointer' : 'auto')};
  line-height: 0;
  padding-left: 43px;
  padding-right: 43px;
  padding-bottom: 20px;
  padding-top: 20px;
  position: relative;
  width: 100%;
  margin-bottom: 1vh;

  min-height: 5vh;
  max-height: 10vh;
  overflow-y: auto;

  /* Works on Firefox */
  scrollbar-width: 6px;
  scrollbar-color: #E7EBF1 #FDB913;

  /* Works on Chrome, Edge, and Safari */
  &::-webkit-scrollbar {
    width: 6px;
  }

  &::-webkit-scrollbar-track {
    background: #E7EBF1;
  }

  &::-webkit-scrollbar-thumb {
    background-color: #FDB913;;
    border-radius: 100px;
    border: 3px solid orange;
  }
`;
