import styled from 'styled-components';

export default styled.div`
  font-size: 1.1vw;
  line-height: calc(2vw);
  // max-width: 800px;
  overflow-y: auto;
  padding: 0 25px;
  width: 100%;
`;
