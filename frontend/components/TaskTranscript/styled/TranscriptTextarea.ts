import styled from 'styled-components';

import Microphone from 'components/svg/Microphone';

export default styled.textarea`
  background-color: transparent;
  height: ${({ height }) => (height ? `${height}px` : 'auto')};
  line-height: 24px;
  max-width: 800px;
  max-height: 380px;
  overflow-y: auto;
  padding: 0 25px;
  resize: none;
  width: 100%;
`;
