import styled from 'styled-components';

import Microphone from 'components/svg/Microphone';

export default styled(Microphone)`
  max-width: 520px;
  position: absolute;
  top: 50%;
  left: 0;
  transform: translateY(-50%);
  width: 100%;
  pointer-events: none;
`;
