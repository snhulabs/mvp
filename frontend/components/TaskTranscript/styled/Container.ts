import styled from 'styled-components';

export default styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding-left: 15px;
  padding-right: 15px;
  position: relative;
  max-width: 1200px;
  width: 100%;
`;
