import React, { useState } from 'react';

import TryAgainPopup from 'components/TryAgainPopup';
import UploadingBar from 'components/UploadingBar';
import ActionButton from 'components/ActionButton';

import Container from './styled/Container';
import Description from './styled/Description';
import Buttons from './styled/Buttons';
import TryAgainButton from './styled/TryAgainButton';
import Transcript from './styled/Transcript';
import TranscriptTextarea from './styled/TranscriptTextarea';
import TranscriptAction from './styled/TranscriptAction';
import TranscriptWrapper from './styled/TranscriptWrapper';
import AudioAnalyser from 'components/AudioAnalyser';

export type TaskTranscriptProps = {
  speechContext?: string;
  transcription: string;
  smileData: any;
  onRetry(): void;
  attempt: number;
  canTryAgain(): boolean;
  audioSrc: string;
  onSubmit(modifiedTranscription: string): void;
  upload(duration: number, speechContext?: string): Promise<void>;
  isVisible: boolean;
};

enum PROGRESS {
  Proceed,
  Uploading,
}

const descriptionCopy = (
  <>
    Listen back to your speech paying attention to the feedback and transcript below.
    As you play through your speech, the visualizations will change over time.
    <br key="break" />
    <br key="break1" />
    Reflect on the visualizations and correct any misinterpretations in the transcript.
    Once you are satisfied with your speech, click Submit to move on.
  </>
);

var waveformObj = null;

const TaskTranscript = ({
  speechContext,
  transcription,
  smileData,
  canTryAgain,
  attempt,
  audioSrc,
  onSubmit,
  onRetry,
  upload,
  isVisible,
}: TaskTranscriptProps): JSX.Element => {
  const audioRef = React.useRef(null);
  const transcriptionRef = React.useRef(null);
  const [height, setHeight] = React.useState<number>(0);
  const [playerIndex, setPlayerIndex] = React.useState<number>(1);
  const [tryAgainOpen, setTryAgainPopup] = useState(false);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [modifiedTranscription, setModifiedTranscription] =
    useState(transcription);
  const [progress, setProgress] = useState(PROGRESS.Proceed);

  const toggleTryAgainPopup = () => {
    setTryAgainPopup(!tryAgainOpen);
  };

  const onTranscriptAction = () => {
    setEditMode(!editMode);
  };

  const updateTranscription = evt => {
    const newTranscription = evt.currentTarget.value;

    setModifiedTranscription(newTranscription);
  };

  const renderActions = () => {
    switch (progress) {
      case PROGRESS.Proceed:
        return (
          <Buttons>
            {canTryAgain() && (
              <TryAgainButton onClick={toggleTryAgainPopup}>
                Try again
              </TryAgainButton>
            )}
            <ActionButton
              onClick={() => {
                setPlayerIndex(currentIndex => currentIndex + 1);
                setProgress(PROGRESS.Uploading);
              }}
            >
              Submit transcript
            </ActionButton>
          </Buttons>
        );
      case PROGRESS.Uploading:
        return (
          <UploadingBar setUploaded={() => onSubmit(modifiedTranscription)} />
        );
      default:
        return null;
    }
  };

  const uploadAudioContext = (duration: number) => {
    if (duration > 0) {
      upload(duration, speechContext);
    }
  };

  const setWaveform = waveform => {
    waveformObj = waveform;
  };

  React.useEffect(() => {
    if ((transcription || modifiedTranscription) && !editMode) {
      const { height: transcriptHeight } =
        transcriptionRef.current.getBoundingClientRect();

      setHeight(transcriptHeight);
    }
  }, [transcription, modifiedTranscription, editMode]);

  React.useEffect(() => {
    if (waveformObj) {
      waveformObj.empty();
      waveformObj.drawBuffer();
    }
  }, [transcription]);

  return (
    <>
      {tryAgainOpen && (
        <TryAgainPopup
          attempt={attempt}
          onAccept={() => {
            if (waveformObj) {
              waveformObj.stop();
            }
            onRetry();
            setTryAgainPopup(false);
          }}
        />
      )}
      <Container>
        {progress === PROGRESS.Uploading ? (
          <Description>Session uploading</Description>
        ) : (
          <Description>{descriptionCopy}</Description>
        )}
        <AudioAnalyser
          waveform={setWaveform}
          audioSrc={audioSrc}
          smileData={smileData}
          onReady={uploadAudioContext}
          isVisible={isVisible}
        />

        <TranscriptWrapper
          hasAction={!editMode}
          onClick={() => {
            if (!editMode) onTranscriptAction();
          }}
        >
          {editMode && (
            <TranscriptAction
              onClick={() => {
                onTranscriptAction();
              }}
            >
              SUBMIT CHANGES
            </TranscriptAction>
          )}
          {editMode ? (
            <TranscriptTextarea
              autoFocus
              value={modifiedTranscription || transcription}
              onChange={updateTranscription}
              height={height}
            />
          ) : (
            <Transcript ref={transcriptionRef}>
              {modifiedTranscription || transcription}
            </Transcript>
          )}
        </TranscriptWrapper>
      </Container>
      {renderActions()}
    </>
  );
};

TaskTranscript.displayName = 'TaskTranscript';

export default TaskTranscript;
