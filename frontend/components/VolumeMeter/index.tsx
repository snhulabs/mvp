import React, { useEffect } from 'react';
import * as d3 from 'd3';

let audioAnalyzer = null;
let dataArray: Uint8Array;
let volume = 0;
let rafId = null;

const height = 180;
const width = 200
const yAxisOffset = 50
const yellowColour = '#F6BC19'
const greyColour = '#E7EBF1'
const lineWidth = 3

const updateTimeThreshold = 300;
const normalizeRange = [0, 75];
const animationDuration = 200;

var heightPreviousValue = 0;
var yPreviousValue = height;

var previousTime = 0;
var averageVolume = 0;
var samplesCount = 0;
var timerCounter = 0;

const domain = [
  "Loud",
  "Strong",
  "Average",
  "Soft",
  "Quiet",
]

function VolumeMeter({ audioContext, inputSource, isActive }) {
  const svgRef = React.useRef();

  const processAudio = event => {
    audioAnalyzer.getByteFrequencyData(dataArray);
    const length = dataArray.length;
    let total = 0;

    for (let i = 0; i < length; i++) {
      total += dataArray[i];
    }
    volume = total / length;
  };

  const clearAnimationFrame = () => {
    if (rafId) {
      window.cancelAnimationFrame(rafId);
      rafId = null;
    }
  }

  useEffect(() => {
    if (!isActive) {
      updateGraph(0);
      clearAnimationFrame();
    }

    return () => {
      clearAnimationFrame();
    }
  }, [isActive])

  useEffect(() => {
    if (audioContext == null || inputSource == null) return;

    // We will need the analyzer for emitting data updates.
    // So we use an instance variable.
    audioAnalyzer = audioContext.createAnalyser();
    inputSource.connect(audioAnalyzer);
    dataArray = new Uint8Array(audioAnalyzer.frequencyBinCount);

    // script processor is used as an intermediary between analyzer
    // and audioContext destination to avoid feedback
    // through microphone.
    // CAUTION: ScriptProcessorNode is deprecated and soon some other technique would be needed to avoid feedback.
    const scriptProcessor = audioContext.createScriptProcessor();
    scriptProcessor.onaudioprocess = processAudio;

    audioAnalyzer.connect(scriptProcessor);
    scriptProcessor.connect(audioContext.destination);

    const drawLoop = (currentTime) => {
      if (audioAnalyzer.ended) return;

      if (timerCounter >= updateTimeThreshold && samplesCount > 0) {
        averageVolume = averageVolume / samplesCount;

        updateGraph(averageVolume)

        averageVolume = 0;
        samplesCount = 0;
        timerCounter = 0;
      }
      else {
        averageVolume += volume;
        samplesCount++;

        timerCounter += currentTime - previousTime;
      }

      previousTime = currentTime;

      rafId = window.requestAnimationFrame(drawLoop);
    };

    requestAnimationFrame(drawLoop);
  }, [inputSource]);

  useEffect(() => {
    const svg = d3.select(svgRef.current)
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr('viewBox', `0 0 ${width} ${height}`)
      // .attr('width', "16vw")
      // .attr('height', "16vw");

    const yScale = d3.scaleBand()
      .domain(domain)
      .range([0, height])

    const yAxis = d3.axisRight(yScale);

    svg
      .select('.y-axis')
      .style('transform', `translateX(${yAxisOffset}px)`)
      .attr('class', 'volume-graph-tick')
      .call(yAxis)
      .select('.domain').remove();

    svg
      .append('line')
      .attr('class', 'grey-line')
      .attr('x1', 20)
      .attr('x2', 20)
      .attr('y1', 0)
      .attr('y2', height)
      .attr('stroke', greyColour)
      .attr('stroke-width', lineWidth);

    svg
      .append('defs')
      .append('clipPath')
      .attr('id', 'meter-clip')
      .append('rect')
      .attr('class', 'clip-path-rect')
      .attr('width', '20px')
      .attr('height', 0)
      .attr('x', 10)
      .attr('y', height)

    const container = svg
      .append('g')
      .attr('width', 20)
      .attr('x', 20)
      .attr('y', 0)
      .attr('clip-path', 'url(#meter-clip)')

    const barsCount = Math.floor(height / 5);
    for (var i = 0; i < barsCount; i++) {
      container.append('rect')
        .attr('width', '10px')
        .attr('height', '2px')
        .attr('fill', yellowColour)
        .attr('x', 20 - 5)
        .attr('y', i * 5)
    }

    svg
      .select('.volume-graph-caption')
      .style('transform', `translate(${width / 2}px, ${height + 30}px)`)
      .style('text-anchor', 'middle');
  }, [])

  const updateGraph = (volume: number) => {
    const percentage = clamp(invert(normalize(volume)), normalizeRange[0], normalizeRange[1]);
    updateClipPath(percentage * height);
  }

  const normalize = (volume: number) => {
    return (volume - normalizeRange[0]) / (normalizeRange[1] - normalizeRange[0])
  }

  const clamp = (value: number, min: number, max: number) => {
    return Math.min(Math.max(value, min), max);
  }

  const invert = (value: number) => {
    return 1 - value;
  }

  const updateClipPath = (position: number) => {
    d3
      .select('.clip-path-rect')
      .transition()
      .ease(d3.easeQuad)
      .duration(animationDuration)
      .attrTween('height', heightTween(position))
      .attrTween('y', yTween(position))
  }

  const heightTween = (value: number) => {
    return function () {
      const newValue = value;
      var interpolate = d3.interpolate(heightPreviousValue, newValue);

      heightPreviousValue = newValue;

      return function (t: number) {
        return clamp(height - interpolate(t), 0, height);
      }
    }
  }

  const yTween = (value: number) => {
    return function () {
      const newValue = value;
      var interpolate = d3.interpolate(yPreviousValue, newValue);

      yPreviousValue = newValue;

      return function (t: number) {
        return interpolate(t);
      }
    }
  }

  return (
    <svg ref={svgRef} className="volume-graph">
      <g className="y-axis" />
      <text className="volume-graph-caption">Target Volume</text>
    </svg>
  );
}

export default VolumeMeter;
