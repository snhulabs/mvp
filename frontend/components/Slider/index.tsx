import React, { FunctionComponent } from 'react';

import Previous from 'components/svg/Previous';
import Next from 'components/svg/Next';

import Bullet from './styled/Bullet';
import Bullets from './styled/Bullets';
import Content from './styled/Content';
import Host from './styled/Host';
import NextWrapper from './styled/NextWrapper';
import PreviousWrapper from './styled/PreviousWrapper';
import Wrapper from './styled/Wrapper';
import BulletTitle from './styled/BulletTitle';
import BulletWrapper from './styled/BulletWrapper';

export interface SliderProps {
  children: React.ReactNode | React.ReactNode[];
  numberOfItems: number;
  activeIndex: number;
  setActiveIndex(activeIndex): void;
  bulletTitle: string;
}

const Slider: FunctionComponent<SliderProps> = ({
  children,
  numberOfItems,
  activeIndex,
  setActiveIndex,
  bulletTitle,
}) => {
  const previous = () => {
    if (activeIndex <= 0) return;

    setActiveIndex(activeIndex - 1);
  };

  const next = () => {
    if (activeIndex + 1 >= numberOfItems) return;

    setActiveIndex(activeIndex + 1);
  };

  const onClick = currentIndex => {
    setActiveIndex(currentIndex);
  };

  return (
    <Host>
      <Wrapper>
        <PreviousWrapper onClick={previous}>
          <Previous color={activeIndex <= 0 ? '#F3F5F8' : '#FDB913'} />
        </PreviousWrapper>

        <Content>{children}</Content>

        <NextWrapper onClick={next}>
          <Next
            color={activeIndex + 1 >= numberOfItems ? '#F3F5F8' : '#FDB913'}
          />
        </NextWrapper>
      </Wrapper>

      <Bullets>
        {[...Array(numberOfItems)].map((_, index) => (
          <BulletWrapper key={index}>
            {activeIndex === index && (
              <BulletTitle
                animate={{ opacity: 1 }}
                initial={{ opacity: 0 }}
                transition={{ duration: 0.5 }}
              >
                {bulletTitle}
              </BulletTitle>
            )}
            <Bullet
              active={activeIndex === index}
              onClick={() => onClick(index)}
            />
          </BulletWrapper>
        ))}
      </Bullets>
    </Host>
  );
};

export default Slider;
