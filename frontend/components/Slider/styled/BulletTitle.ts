import styled from 'styled-components';
import { motion } from 'framer-motion';

export default styled(motion.div)`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 100%;
`;
