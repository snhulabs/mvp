import styled from 'styled-components';

import { DEEP_LEVEL } from 'config';

export default styled.div`
  width: 100%;
  z-index: ${DEEP_LEVEL[1]};

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
`;
