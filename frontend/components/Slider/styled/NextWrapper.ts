import styled from 'styled-components';

export default styled.div`
  width: 20rem;
  cursor: pointer;
`;
