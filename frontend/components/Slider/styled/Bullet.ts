import styled from 'styled-components';

export default styled.div`
  position: relative;
  width: 1.5rem;
  height: 1.5rem;
  margin: 0 2rem;

  border: solid 2px
    ${({ theme, active }) => (active ? theme.colors.action : theme.colors.gray)};
  border-radius: 50%;
  cursor: pointer;

  transition: border-color 0.5s ease;
`;
