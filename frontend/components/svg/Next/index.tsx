import React from 'react';

export default function Next({ color }) {
  return (
    <svg
      width="62"
      height="62"
      viewBox="0 0 62 62"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="31" cy="31" r="30" stroke={color} strokeWidth="2" />
      <rect
        x="27.8027"
        y="20"
        width="15"
        height="2"
        rx="1"
        transform="rotate(45 27.8027 20)"
        fill={color}
      />
      <rect
        x="26"
        y="40.2168"
        width="15"
        height="2"
        rx="1"
        transform="rotate(-45 26 40.2168)"
        fill={color}
      />
    </svg>
  );
}
