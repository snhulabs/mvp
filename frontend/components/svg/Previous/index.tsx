import React from 'react';

export default function Previous({ color }) {
  return (
    <svg
      style={{float: 'right'}}
      width="62"
      height="62"
      viewBox="0 0 62 62"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle
        r="30"
        transform="matrix(-1 0 0 1 31 31)"
        stroke={color}
        strokeWidth="2"
      />
      <rect
        width="15"
        height="2"
        rx="1"
        transform="matrix(-0.707107 0.707107 0.707107 0.707107 34.1973 20)"
        fill={color}
      />
      <rect
        width="15"
        height="2"
        rx="1"
        transform="matrix(-0.707107 -0.707107 -0.707107 0.707107 36 40.2168)"
        fill={color}
      />
    </svg>
  );
}
