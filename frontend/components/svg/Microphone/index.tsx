import React from 'react';
import { motion } from 'framer-motion';

import Svg from './styled/Svg';

export default function Microphone({
  className,
  isActive = false,
  isChecked = false,
  isCrossed = false,
  isCountdown = false,
  animateOutlines = true,
  recordingStopTime = 0,
  time = 0,
}) {
  return (
    <Svg
      className={className}
      width="656"
      height="656"
      viewBox="0 0 656 656"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <motion.g
        id="microphone"
        animate={{ opacity: 1 }}
        initial={{ opacity: 0 }}
        transition={{ delay: 0.5 }}
      >
        <path
          d="M330.73,396.36a60.85,60.85,0,0,0,60.64-60.92V227.85a60.64,60.64,0,1,0-121.28,0h0V335.44A60.85,60.85,0,0,0,330.73,396.36ZM281,227.85a49.71,49.71,0,1,1,99.42,0V335.44a49.71,49.71,0,1,1-99.42.05V227.85Z"
          fill="#0A3370"
          opacity="0.05"
        />
        <path
          d="M418.34,285.51a5.47,5.47,0,0,0-5.46,5.48h0v50.11c0,45.51-36.86,82.53-82.15,82.53s-82.14-37-82.14-82.53V291a5.47,5.47,0,0,0-10.93-.47,3.69,3.69,0,0,0,0,.47v50.11c0,49.72,38.82,90.5,87.61,93.35v59.31H295.83a5.5,5.5,0,1,0,0,11h69.8a5.5,5.5,0,0,0,0-11H336.19V434.46c48.79-2.85,87.61-43.63,87.61-93.35V291a5.47,5.47,0,0,0-5.45-5.49Z"
          fill="#0A3370"
          opacity="0.05"
        />
      </motion.g>
      {isActive && animateOutlines && (
        <g id="outlines">
          <motion.g
            animate={{
              opacity: [0, 1, 0],
            }}
            transition={{
              yoyo: Infinity,
              duration: 3,
            }}
          >
            <motion.circle
              id="outline"
              opacity="0.03"
              cx="328"
              cy="328"
              r="268"
              stroke="#0A3370"
              strokeWidth="10"
            />
          </motion.g>
          <motion.g
            animate={{
              opacity: [0, 1, 0],
            }}
            transition={{
              yoyo: Infinity,
              duration: 3,
              delay: 0.5,
            }}
          >
            <motion.circle
              id="outline-2"
              opacity="0.02"
              cx="328"
              cy="328"
              r="323"
              stroke="#0A3370"
              strokeWidth="10"
            />
          </motion.g>
        </g>
      )}
      {!isCrossed && (
        <motion.path
          id="progress"
          d="M555.54,328c0,124.16-100.65,224.81-224.81,224.81S105.92,452.16,105.92,328,206.57,103.19,330.73,103.19,555.54,203.84,555.54,328Z"
          fill="none"
          stroke="#0a3370"
          strokeWidth="11"
          fillRule="evenodd"
          opacity={0.05}
        />
      )}
      {isCountdown && (
        <motion.path
          animate={{ pathLength: isActive ? 1 : recordingStopTime }}
          transition={{ duration: isActive ? time / 1000 : 0, ease: 'linear' }}
          initial={{ pathLength: recordingStopTime, rotate: -90 }}
          id="progressYellow"
          d="M555.54,328c0,124.16-100.65,224.81-224.81,224.81S105.92,452.16,105.92,328,206.57,103.19,330.73,103.19,555.54,203.84,555.54,328Z"
          fill="none"
          stroke="#FDB913"
          strokeWidth="11"
          fillRule="evenodd"
          opacity="0.5"
        />
      )}
      {isChecked && (
        <motion.path
          id="checked"
          initial={{ pathLength: 0 }}
          animate={{ pathLength: 1 }}
          transition={{ duration: 1, ease: 'easeOut', delay: 0.5 }}
          d="M184.31,340.83l91,91.16a15.07,15.07,0,0,0,23.35-2.51l151.58-236.2"
          fill="none"
          stroke="#0a3370"
          strokeWidth="11"
          fillRule="evenodd"
          opacity="0.05"
        />
      )}
      {isCrossed && (
        <motion.path
          id="crossed"
          initial={{ pathLength: 0 }}
          animate={{ pathLength: 1 }}
          transition={{ duration: 1, ease: 'easeOut', delay: 0.5 }}
          d="M163.18,178.11,480.63,495.56m17.65-317.45L180.83,495.56"
          fill="none"
          stroke="#0a3370"
          strokeWidth="11"
          fillRule="evenodd"
          opacity="0.05"
        />
      )}
    </Svg>
  );
}
