import styled from 'styled-components';

export default styled.img`
  width: ${({ size }) => size}%;
  margin: 1rem;
`;
