import styled, { css } from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  font-family: Palatino;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;

  ${({ inline }) =>
    inline &&
    css`
      flex-direction: row;
    `};
`;
