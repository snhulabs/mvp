import styled, { css } from 'styled-components';

export default styled.p`
  ${({ inline }) =>
    inline &&
    css`
      order: 1;
      text-align: start;
    `};
`;
