import React, { FunctionComponent, useState } from 'react';

import Modal from 'components/Modal';
import Slider from 'components/Slider';
import Close from 'components/svg/Close';

import Description from './styled/Description';
import Title from './styled/Title';
import CloseWrapper from './styled/CloseWrapper';
import Content from './styled/Content';
import Image from './styled/Image';
import Descriptions from './styled/Descriptions';

interface TroubleshootingProps {
  setIsOpenModal(isOpenModal): void;
}

const Troubleshooting: FunctionComponent<TroubleshootingProps> = ({
  setIsOpenModal,
}) => {
  const troubleshootingData = [
    {
      description:
        'Ensure that you have a microphone (mic) in your system. If you have ever made a successful Zoom or Skype call on this computer, then you probably have a mic.  <br /> <br /> <br /> On many computers, like laptops, the mic is built into the computer. If the mic is external (mostly desktop computers), ensure it is plugged in and, if applicable, turned on.  <br /> <br /> <br /> Check your operating system settings to make sure that the system is using your microphone as an input device.',
      bulletTitle: 'Hardware',
    },
    {
      description:
        'For Windows: Right-click the volume icon on the bottom right-hand side of your screen and click Open Sound Settings. Ensure your microphone is selected as the Input device.',
      bulletTitle: 'Windows',
      image: {
        src: '/assets/images/troubleshooting_windows.png',
        size: 100,
        inline: false,
      },
    },
    {
      description:
        'For MacOS: Hold the ‘option’ key on your keyboard and click the volume icon at the top of your screen. Select your microphone in the ‘Input Device’ section',
      description2:
        'If you don’t see any Input Device section, click Sound Preferences, go to the Input tab, and select your microphone there.',
      bulletTitle: 'Mac OS',
      image: {
        src: '/assets/images/troubleshooting_macOS.png',
        size: 45,
        inline: false,
      },
    },
    {
      description:
        'Ensure that you gave your browser permission to access the microphone. For most browsers, this can be set by clicking the padlock icon to the left of the address bar, and then click ‘Site Settings’',
      description2:
        'In the following screen, ensure that microphone access is set to ALLOW and <b>not</b> blocked ',
      description3:
        'If you continue to have issues, consider using a different browser, we recommend Google Chrome.',
      bulletTitle: 'Browser',
      image: {
        src: '/assets/images/troubleshooting_browser1.png',
        size: 50,
        inline: true,
      },
      image2: { src: '/assets/images/troubleshooting_browser2.png', size: 90 },
    },
  ];
  const [activeIndex, setActiveIndex] = useState<number>(0);

  return (
    <Modal>
      <CloseWrapper onClick={() => setIsOpenModal(false)}>
        <Close />
      </CloseWrapper>

      <Slider
        activeIndex={activeIndex}
        setActiveIndex={setActiveIndex}
        bulletTitle={troubleshootingData[activeIndex].bulletTitle}
        numberOfItems={troubleshootingData.length}
      >
        <Title>Troubleshooting tips</Title>
        {troubleshootingData.map((data, index) => (
          <>
            {activeIndex === index && (
              <Content
                key={index}
                animate={{ opacity: 1 }}
                initial={{ opacity: 0 }}
                transition={{ duration: 0.5 }}
              >
                <Descriptions
                  {...(data.image && { inline: data.image.inline })}
                >
                  <Description
                    {...(data.image && { inline: data.image.inline })}
                    dangerouslySetInnerHTML={{
                      __html: data.description,
                    }}
                  />
                  {data.image && (
                    <Image size={data.image.size} src={data.image.src} />
                  )}
                </Descriptions>

                {data.description2 && (
                  <Description
                    dangerouslySetInnerHTML={{
                      __html: data.description2,
                    }}
                  />
                )}
                {data.image2 && (
                  <Image size={data.image2.size} src={data.image2.src} />
                )}
                {data.description3 && (
                  <Description
                    dangerouslySetInnerHTML={{
                      __html: data.description3,
                    }}
                  />
                )}
              </Content>
            )}
          </>
        ))}
      </Slider>
    </Modal>
  );
};

export default Troubleshooting;
