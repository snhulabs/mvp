import React from 'react';
import type { AppProps } from 'next/app';
import { ThemeProvider } from 'styled-components';
import { AnimatePresence } from 'framer-motion';
import * as Sentry from '@sentry/browser';

import '../styles/font.css';
import '../styles/volume-meter.css'
import '../styles/bar-graph.css'

import Version from 'components/Version';
import GlobalStyles from 'utils/global-styles';
import theme from 'utils/theme';
import { AudioProvider } from 'context/audio.context';
import { AppUserProvider } from 'context/appUser.context';
import { NavigationProvider } from 'context/navigation.context';
import UserIdentifierLabel from 'components/UserIdentifierLabel';

Sentry.init({
  // tslint:disable-next-line: no-any
  enabled: process.env.NODE_ENV !== 'development',
  dsn: process.env.SENTRY_DSN,
});

const App = ({ Component, pageProps, router }: AppProps) => (
  <ThemeProvider theme={theme}>
    <AudioProvider>
      <AppUserProvider>
        <NavigationProvider>
          <GlobalStyles />
          <AnimatePresence exitBeforeEnter={true}>
            <Component key={router.route} router={router} {...pageProps} />
          </AnimatePresence>
          <Version />
          <UserIdentifierLabel />
        </NavigationProvider>
      </AppUserProvider>
    </AudioProvider>
  </ThemeProvider>
);

export default App;
