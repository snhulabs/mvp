export { default, getStaticProps } from 'containers/SubmitPage';
import strapiI18n from 'utils/i18n/strapi';

export async function getStaticPaths() {
  return strapiI18n.getLocalePaths();
}
