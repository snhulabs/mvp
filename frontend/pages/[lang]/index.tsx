export { default, getStaticProps } from 'containers/LandingPage';
import strapiI18n from 'utils/i18n/strapi';

export async function getStaticPaths() {
  return strapiI18n.getLocalePaths();
}
