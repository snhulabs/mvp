export { default, getStaticProps } from 'containers/MicrophoneTwoPage';
import strapiI18n from 'utils/i18n/strapi';

export async function getStaticPaths() {
  return strapiI18n.getLocalePaths();
}
