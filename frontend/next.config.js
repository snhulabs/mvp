const withSourceMaps = require('@zeit/next-source-maps');
const withOptimizedImages = require('next-optimized-images');
// const SentryWebpackPlugin = require('@sentry/webpack-plugin');

const env = require('./env');

module.exports = withOptimizedImages(
  withSourceMaps({
    env,
    serverRuntimeConfig: {
      ...env,
    },
    webpack: config => {
      return config;
    },
  })
);
