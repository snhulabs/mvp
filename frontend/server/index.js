const express = require('express');
const next = require('next');
const basicAuth = require('express-basic-auth');
const bodyParser = require('body-parser');

const config = require('./utils/config');
const i18nProxy = require('./proxies/i18nProxy');
const forceHttpsMiddleware = require('./middlewares/forceHttps');
const initialLocaleMiddleware = require('./middlewares/initialLocale');
const trailingSlashMiddleware = require('./middlewares/trailingSlash');

const port = process.env.PORT || 3000;
const dev = process.env.NODE_ENV === 'local';
const app = next({ dev });

const handle = app.getRequestHandler();

(async () => {
  await app.prepare();
  const server = express();
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: true }));

  if (
    !dev &&
    config.BASIC_AUTH_PASSWORD !== '' &&
    config.BASIC_AUTH_PASSWORD !== undefined &&
    config.BASIC_AUTH_PASSWORD !== 'disable'
  ) {
    const basicAuthConfig = {
      users: {
        [config.BASIC_AUTH_USERNAME]: config.BASIC_AUTH_PASSWORD,
      },
      challenge: true,
    };
    server.use(basicAuth(basicAuthConfig));
  }

  if (!config.DISABLE_I18N) {
    server.use(initialLocaleMiddleware);
  }

  server.use(forceHttpsMiddleware);
  server.use(trailingSlashMiddleware);

  server.get('*', (req, res) => {
    const proxyReq = i18nProxy(req);
    return handle(proxyReq, res);
  });

  await server.listen(port);
  console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line no-console
})();
