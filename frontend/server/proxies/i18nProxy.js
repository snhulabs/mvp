const config = require('../utils/config');
const {
  isRouteUrl,
  isDataUrl,
  prefixRouteUrl,
  prefixDataUrl,
} = require('../utils/url');

module.exports = function i18nProxy(req) {
  if (!config.DISABLE_I18N) {
    return req;
  }

  if (isRouteUrl(req.url)) {
    // Proxy all route requests to default locale routes
    return {
      ...req,
      url: prefixRouteUrl(req.url, config.DEFAULT_LOCALE),
    };
  }

  if (isDataUrl(req.url)) {
    // Proxy all data requests to default locale data url
    return {
      ...req,
      url: prefixDataUrl(req.url, config.DEFAULT_LOCALE),
    };
  }

  return req;
};
