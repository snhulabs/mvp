module.exports = function forceHttps(req, res, next) {
  if (
    !req.secure &&
    req.get('x-forwarded-proto') !== 'https' &&
    process.env.ENV === 'production'
  ) {
    return res.redirect(`https://${req.get('host')}${req.url}`);
  }
  next();
};
