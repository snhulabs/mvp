const ApolloClient = require('apollo-boost').default;
const gql = require('apollo-boost').gql;
const fetch = require('isomorphic-unfetch');
const geoip = require('geoip-lite');

const config = require('../utils/config');

let supportedLocales = null;

const cmsApiClient = new ApolloClient({
  uri: config.CMS_GRAPHQL_URL,
  fetch,
});

function getDefaultLocale() {
  return config.DEFAULT_LOCALE;
}

function getBrowserLocales(req) {
  const locales = req.headers['accept-language'];
  if (!locales) {
    return [];
  }

  return locales
    .split(/[;,]/)
    .filter(lng => lng.indexOf('=') === -1)
    .map(lng => lng.toLowerCase());
}

function getIpLocale(req) {
  const ip =
    (req.headers['x-forwarded-for'] || '').split(',')[0] ||
    req.headers['x-appengine-user-ip'] ||
    req.connection.remoteAddress;

  console.log('IP:', ip);

  const geo = geoip.lookup(ip);
  if (!geo) {
    return null;
  }

  const country = geo.country.toLowerCase();
  console.log('IP:', ip);
  console.log('Country:', country);
  const code = config.GEOIP_MAPPING[country];
  return code;
}

function getSupportedLocales() {
  if (supportedLocales) {
    return Promise.resolve(supportedLocales);
  }

  return cmsApiClient
    .query({
      query: gql`
        {
          languages {
            code
          }
        }
      `,
    })
    .then(result => {
      supportedLocales = result.data.languages.map(l => l.code.toLowerCase());
      return supportedLocales;
    });
}

function selectLocale(
  locales,
  supportedLocales,
  fallbackLocale,
  matchSameLanguage = true
) {
  const getLocaleLanguage = locale => locale.split('-')[0];
  const getLocaleListLanguages = localeList =>
    localeList.map(getLocaleLanguage);

  // Include partial matches (eg. 'es-ES' or 'es' for 'es' / 'en', 'en-US' or 'en-GB' for 'en-US')
  let sameLanguageLocale;
  if (matchSameLanguage) {
    // Keep the user's language preference order
    const userPreferedLanguageMatch = locales.find(locale =>
      getLocaleListLanguages(supportedLocales).includes(
        getLocaleLanguage(locale)
      )
    );

    sameLanguageLocale =
      userPreferedLanguageMatch &&
      supportedLocales.find(
        locale =>
          getLocaleLanguage(userPreferedLanguageMatch) ===
          getLocaleLanguage(locale)
      );
  }

  // Find exact match only (eg. only 'es' for 'es' / only 'en-US' for 'en-US')
  const exactLocale = locales.find(locale => supportedLocales.includes(locale));

  return sameLanguageLocale || exactLocale || fallbackLocale;
}

function getInitialLocale(req) {
  return getSupportedLocales().then(supportedLocales => {
    const defaultLocale = getDefaultLocale();
    console.log('Default locale:', defaultLocale);
    console.log('Supported locales:', supportedLocales);
    let chosenLocale = defaultLocale;

    if (config.USE_BROWSER_LOCALE) {
      const browserLocales = getBrowserLocales(req);
      console.log('Browser locales:', browserLocales);
      chosenLocale = selectLocale(
        browserLocales,
        supportedLocales,
        defaultLocale
      );
    }

    if (config.USE_GEOIP_LOCALE) {
      const ipLocale = getIpLocale(req);
      console.log('GEOIP locale:', ipLocale);
      if (ipLocale) {
        chosenLocale = selectLocale(
          [ipLocale],
          supportedLocales,
          defaultLocale
        );
      }
    }

    console.log('Chosen locale:', chosenLocale);
    return chosenLocale;
  });
}

module.exports = (req, res, next) => {
  if (req.url === '/' && !config.DISABLE_I18N) {
    getInitialLocale(req).then(initialLocale => {
      res.writeHead(301, { Location: `/${initialLocale}` });
      res.end();
    });
  } else {
    next();
  }
};
