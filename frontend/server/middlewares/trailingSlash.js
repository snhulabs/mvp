module.exports = (req, res, next) => {
  if (req.url && req.url !== '/' && req.url.endsWith('/')) {
    res.writeHead(301, { Location: req.url.slice(0, -1) });
    return res.end();
  }
  next();
};
