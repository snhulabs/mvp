export interface I18nProvider {
  getLocalePaths(): Promise<{ paths: string[]; fallback: boolean }>;
}
