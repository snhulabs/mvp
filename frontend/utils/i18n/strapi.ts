import { cmsApiClient } from 'utils/api/cms';
import { gql } from 'apollo-boost';

import { I18nProvider } from 'utils/i18n';

class StrapiI18n implements I18nProvider {
  async getLocalePaths() {
    return cmsApiClient
      .query({
        query: gql`
          {
            languages {
              code
            }
          }
        `,
      })
      .then(result => ({
        paths: result.data.languages.map(l => ({
          params: {
            lang: l.code,
          },
        })),
        fallback: false,
      }));
  }
}

export default new StrapiI18n();
