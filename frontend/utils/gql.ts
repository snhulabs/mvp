import { gql } from 'apollo-boost';

const whereLanguage = (lang: string | string[]): string => {
  return `where: { language: { code: "${lang}" } }`;
};

export const copyQuery = () => gql`
  {
    copies {
        copies
    }
  }
`;
