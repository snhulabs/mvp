import * as React from 'react';

export enum PROGRESS {
  INTRO,
  MIC_RECORD,
  SUCCESS,
  CONFIRM_SUCCESS,
}

const renderMicProgress = (progress, componentsMap = {}) => {
  switch (progress) {
    case PROGRESS.INTRO:
    case PROGRESS.MIC_RECORD:
    case PROGRESS.SUCCESS:
    case PROGRESS.CONFIRM_SUCCESS:
      return componentsMap[progress] || null;
    default:
      return null;
  }
};

export default renderMicProgress;