import { join, trimTrailingSlash, isRemotePath } from './path';

describe('utils/path', () => {
  test.each([
    [['a', 'b', 'c'], 'a/b/c'],
    [['/a', '/b/', 'c'], '/a/b/c'],
    [['this//', 'is', '/a/', 'longer/', 'path'], 'this/is/a/longer/path'],
  ])('join works as expected', (components, expected) => {
    expect(join(components)).toBe(expected);
  });

  test.each([
    ['/this/is/some/path/', '/this/is/some/path'],
    ['https://domain.com/another/path/', 'https://domain.com/another/path'],
  ])('trailing slashes are trimmed', (url, expected) => {
    expect(trimTrailingSlash(url)).toBe(expected);
  });

  test.each([
    ['/this/is/some/path/', false],
    ['this/is/some/path/', false],
    ['about', false],
    ['gallery/1234', false],
    ['https://domain.com/another/path/', true],
    ['http://domain.com/another/path/', true],
    ['//domain.com/another/path/', true],
  ])('remote paths are detected correctly', (url, expected) => {
    expect(isRemotePath(url)).toBe(expected);
  });
});
