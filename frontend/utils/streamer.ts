export const SAMPLE_RATE = 16000;

export default class Streamer {
  private audioContext: AudioContext;
  private scriptProcessor: ScriptProcessorNode;
  private audioSource: MediaStreamAudioSourceNode;

  private stream: MediaStream;

  public isStreaming = false;
  public onUpdate = evt => {};
  public onFinished = dataBuffer => {};

  constructor(stream, onUpdate, onFinished) {
    this.onUpdate = onUpdate;
    this.onFinished = onFinished;
    this.stream = stream;

    this.init();
  }

  init = async () => {
    if (this.stream) {
      // @ts-ignore
      this.audioContext = new (window.AudioContext || window.webkitAudioContext)({
        sampleRate: SAMPLE_RATE,
      });

      this.scriptProcessor = this.audioContext.createScriptProcessor(
        2048,
        1,
        1
      );
      this.audioSource = this.audioContext.createMediaStreamSource(this.stream);
    }
  };

  start = () => {
    console.log('started');
    this.scriptProcessor.connect(this.audioContext.destination);
    this.scriptProcessor.addEventListener('audioprocess', this.onStream);
    this.audioSource.connect(this.scriptProcessor);

    this.isStreaming = true;
  };

  pause = () => {
    this.isStreaming = false;
  };

  stop = () => {
    console.log('stop recording');
    try {
      this.audioSource.disconnect(this.scriptProcessor);
      this.scriptProcessor.disconnect(this.audioContext.destination);
      this.scriptProcessor.removeEventListener('audioprocess', this.onStream);

      this.onFinished(null);
    } catch (e) {
      console.log('Error stopping');
    }
  };

  destroy = () => {
    try {
      this.audioSource.disconnect(this.scriptProcessor);
      this.scriptProcessor.disconnect(this.audioContext.destination);
      this.scriptProcessor.removeEventListener('audioprocess', this.onStream);
      this.audioContext.close();
    } catch (e) {
      console.log('Error destroying');
    }
  };

  onStream = event => {
    if (this.isStreaming) {
      const input = event.inputBuffer.getChannelData(0);

      this.onUpdate(input);
    }
  };
}
