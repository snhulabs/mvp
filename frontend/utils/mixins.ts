import { DEEP_LEVEL } from 'config';

export const setBufferingWrapper = () => `
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: ${DEEP_LEVEL[2]};
`;
