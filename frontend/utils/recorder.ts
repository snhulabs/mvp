export const SAMPLE_RATE = 16000;

export default class Recorder {
  private audioContext: AudioContext;
  private scriptProcessor: ScriptProcessorNode;
  private audioSource: MediaStreamAudioSourceNode;
  private mediaRecorder: any;

  private stream: MediaStream;
  private chunks = [];

  public isRecording = false;
  public onUpdate = evt => {};
  public onFinished = (dataBuffer, chunks) => {};

  constructor(stream, onUpdate, onFinished) {
    this.onUpdate = onUpdate;
    this.onFinished = onFinished;
    this.stream = stream;

    this.init();
  }

  init = async () => {
    // @ts-ignore
    this.audioContext = new (window.AudioContext || window.webkitAudioContext)({
      sampleRate: SAMPLE_RATE,
    });

    // @ts-ignore
    this.mediaRecorder = new MediaRecorder(this.stream, {
      mimeType: 'audio/webm',
    });
    this.mediaRecorder.addEventListener('dataavailable', this.saveChunks);
    this.mediaRecorder.addEventListener('stop', this.onMediaRecorderStop);
  };

  saveChunks = event => {
    this.chunks.push(event.data);
  };

  record = () => {
    this.mediaRecorder.start();
    this.isRecording = true;
  };

  pause = () => {
    this.isRecording = false;
  };

  onMediaRecorderStop = () => {
    this.isRecording = false;

    this.onFinished(null, this.chunks);
    this.chunks = [];
  };

  stop = () => {
    console.log('stop recording');
    try {
      this.mediaRecorder.stop();
    } catch {
      console.log('Error stopping');
    }
  };

  destroy = () => {
    try {
      this.chunks = [];
      this.mediaRecorder.remove('dataavailable', this.saveChunks);
      this.mediaRecorder.stop();
    } catch {}
  };
}
