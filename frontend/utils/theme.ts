export default {
  colors: {
    black: '#000000',
    white: '#ffffff',
    background: '#0A3370',
    action: '#FDB913',
    gray: '#F3F5F8',
    grayDark: '#6666666',
    blueOpacity: 'rgba(10, 51, 112, 0.1)',
  },
  fonts: {
    palatino: 'Palatino',
    helvetica: 'Helvetica',
  },
};
