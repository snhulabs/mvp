import ApolloClient, { gql } from 'apollo-boost';
import fetch from 'isomorphic-unfetch';

export const cmsApiClient = new ApolloClient({
  uri:'http://localhost:1337/graphql',
  fetch,
});

export const createAppUser = async appUser => {
  const CREATE_APP_USER = gql`
    mutation CreateAppUser($input: createAppUserInput) {
      createAppUser(input: $input) {
        appUser {
          id
          userId
          step
          device
          browser
        }
        token
      }
    }
  `;

  return cmsApiClient.mutate({
    mutation: CREATE_APP_USER,
    variables: { input: { data: appUser } },
  });
};

export const updateAppUser = async (token, appUserId, appUser) => {
  const UPDATE_APP_USER = gql`
    mutation UpdateAppUser($input: updateAppUserInput) {
      updateAppUser(input: $input) {
        appUser {
          environmentLocation
          step
        }
      }
    }
  `;

  return cmsApiClient.mutate({
    mutation: UPDATE_APP_USER,
    variables: {
      input: {
        data: {
          ...appUser,
          token,
        },
        where: {
          id: appUserId,
        },
      },
    },
  });
};
