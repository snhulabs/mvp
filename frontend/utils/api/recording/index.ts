import LoginForm from "components/LoginForm";

export const api = 'http://localhost:1337';

export const updateRecordingBuffer = (buffer, indexUpload, taskId, token) => {
  //Put request
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ data: buffer, index: indexUpload, token: token }),
  };

  return fetch(`${api}/audioitems/${taskId}`, requestOptions) //need to change that link to something more pretty
    .then(response => response.json());
};

export const startRecording = (token: string) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ token: token }),
  };

  return fetch(`${api}/audioitems`, requestOptions) //need to change that link to something more pretty
    .then(response => response.json());
};

export const sendAudio = async (blob, duration, speechContext) => {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    const data = new FormData();

    if (speechContext) {
      data.append('speech_context', speechContext);
    }

    data.append('duration', duration);
    data.append('audio_data', blob, 'test.wav');

    req.onload = e => {
      resolve(JSON.parse(req.response));
    };

    req.open('POST', `${api}/audio-translations/register`, true);
    req.send(data);
  });
};

export const stopRecording = (taskId, token) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ token: token }),
  };

  return fetch(`${api}/audioitems/count/${taskId}`, requestOptions) //need to change that link to something more pretty
    .then(response => response.json());
};

export const createTask = ({ entryId, title, attempt = 0, token }) => {
  const datetime = new Date().toISOString();
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      app_user: entryId,
      title,
      attempt,
      token,
      date: datetime,
    }),
  };

  return fetch(`${api}/tasks`, requestOptions).then(response =>
    response.json()
  );
};

export const updateTask = ({ audioFile, entryId, text, modifyText, confidence, words, smileData, token }) => {

  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      audioFile,
      text,
      modifyText,
      confidence,
      words,
      smileData,
      token
    }),
  };

  return fetch(`${api}/tasks/${entryId}`, requestOptions).then(response =>
    response.json()
  );
};
