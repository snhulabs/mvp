import { useLocalizedRouter } from 'hooks';
import * as React from 'react';
import { AppUserContext } from './appUser.context';

export enum ROUTING_STEPS {
  ENTER_USER_ID = 'ENTER_USER_ID',
  SETUP_MIC = 'SETUP_MIC',
  MIC_ONE = 'MIC_ONE',
  MIC_TWO = 'MIC_TWO',
  COMPETENCE = 'COMPETENCE',
  LIKEABILITY = 'LIKEABILITY',
  ALL_TOGETHER = 'ALL_TOGETHER',
  END = 'END',
  PROTOTYPE = 'PROTOTYPE',
}

const ROUTES_CONFIG = {
  [ROUTING_STEPS.ENTER_USER_ID]: {
    path: '/login',
    title: 'Enter User ID',
    idx: 1,
  },
  [ROUTING_STEPS.SETUP_MIC]: {
    path: '/setup',
    title: 'Mic Setup',
    idx: 2,
  },
  [ROUTING_STEPS.MIC_ONE]: {
    path: '/micone',
    title: 'Test Microphone',
    idx: 3,
  },
  [ROUTING_STEPS.MIC_TWO]: {
    path: '/mictwo',
    title: 'Test Microphone',
    idx: 3,
  },
  [ROUTING_STEPS.COMPETENCE]: {
    path: '/competence',
    title: 'Sounding Competent',
    idx: -1,
  },
  [ROUTING_STEPS.LIKEABILITY]: {
    path: '/likeability',
    title: 'Sounding Likeable',
    idx: -1,
  },
  [ROUTING_STEPS.ALL_TOGETHER]: {
    path: '/allTogether',
    title: 'Putting It All Together',
    idx: 6,
  },
  [ROUTING_STEPS.END]: {
    path: '/end',
    title: '',
    idx: 7,
  },
  [ROUTING_STEPS.PROTOTYPE]: {
    path: '/prototype',
    title: '',
    idx: 8,
  },
};

export const NavigationContext = React.createContext({
  currentTitle: '',
  currentStep: 1,
  goTo: (step: ROUTING_STEPS) => null,
  setupView: (step: ROUTING_STEPS) => null,
  goToTask: () => null,
});

export const NavigationProvider = ({ children }) => {
  const { userId } = React.useContext(AppUserContext);
  const { path: routerPath, push } = useLocalizedRouter();
  const [currentTask, setCurrentTask] = React.useState<ROUTING_STEPS>(null);
  const [currentStep, setCurrentStep] = React.useState<number>(1);
  const [currentTitle, setCurrentTitle] = React.useState<string>(
    ROUTES_CONFIG[ROUTING_STEPS.ENTER_USER_ID].title
  );

  const goTo = (step: ROUTING_STEPS): void => {
    const { path } = ROUTES_CONFIG[step];
    setCurrentTask(step);
    push(path);
  };

  const setupView = (step: ROUTING_STEPS): void => {
    const stepName = currentTask ? currentTask: step;
    const { title, idx } = ROUTES_CONFIG[stepName];

    const newStepIndex =
      idx === -1 ? (!currentStep ? 4 : currentStep + 1) : idx;

    const isTaskView = [
      ROUTING_STEPS.COMPETENCE,
      ROUTING_STEPS.LIKEABILITY,
      ROUTING_STEPS.ALL_TOGETHER,
    ].includes(step);

    setCurrentTitle(title);
    setCurrentStep(newStepIndex);

    if (isTaskView) {
      setCurrentTask(step);
    }
  };

  const goToTask = (): void => {
    if (currentStep === 5) {
      const { path } = ROUTES_CONFIG[ROUTING_STEPS.ALL_TOGETHER];

      setCurrentTask(ROUTING_STEPS.ALL_TOGETHER);
      push(path);
    } else {
      if (!currentTask) {
        const step =
          Math.random() > 0.5
            ? ROUTING_STEPS.LIKEABILITY
            : ROUTING_STEPS.COMPETENCE;
        const { path } = ROUTES_CONFIG[step];

        setCurrentTask(step);
        push(path);
      } else {
        const step =
          currentTask === ROUTING_STEPS.LIKEABILITY
            ? ROUTING_STEPS.COMPETENCE
            : ROUTING_STEPS.LIKEABILITY;
        const { path } = ROUTES_CONFIG[step];

        setCurrentTask(step);
        push(path);
      }
    }
  };

  React.useEffect(() => {
    const splittedPath = routerPath.split('/');
    const isLanding = splittedPath.length < 3;
    const isLogin = splittedPath.includes('login');

    if (!userId && !isLanding && !isLogin) {
      goTo(ROUTING_STEPS.ENTER_USER_ID);
    }
  }, []);

  return (
    <NavigationContext.Provider
      value={{
        currentTitle,
        currentStep,
        goTo,
        goToTask,
        setupView,
      }}
    >
      {children}
    </NavigationContext.Provider>
  );
};
