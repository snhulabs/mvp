import * as React from 'react';
import { createAppUser, updateAppUser } from 'utils/api/cms';
import Bowser from 'bowser';

export const AppUserContext = React.createContext({
  login: null,
  update: null,
  userId: null,
  entryId: null,
  token: null,
});

const getEnvData = () => {
  const bowser = Bowser.getParser(window.navigator.userAgent);

  return {
    device: bowser.getPlatformType(),
    browser: bowser.getBrowserName(),
  };
};

export const AppUserProvider = ({ children }) => {
  const [entryId, setEntryId] = React.useState<string>('');
  const [userId, setUserId] = React.useState<number>(null);
  const [userStep, setUserStep] = React.useState<number>(1);
  const [token, setToken] = React.useState<string>(null);
  const [environmentLocation, setEnvironmentLocation] = React.useState<string>(
    ''
  );

  React.useEffect(() => {
    const cachedToken = localStorage.getItem('SNHU-user-token');

    if (cachedToken) {
      setToken(cachedToken);
    }
  }, []);

  const login = (appUserId, onSuccessCallback) => {
    createAppUser({
      userId: appUserId,
      step: 1,
      ...getEnvData(),
    })
      .then(({ data }) => {
        const {
          createAppUser: { appUser, token: userToken },
        } = data;

        localStorage.setItem('SNHU-user-token', userToken);

        setUserId(appUser.userId);
        setEntryId(appUser.id);
        setUserStep(appUser.step);
        setToken(userToken);
        onSuccessCallback();
      })
      .catch(() => {
        setUserId(null);
      });
  };

  const update = (appUserData, onSuccessCallback) => {
    updateAppUser(token, entryId, appUserData)
      .then(({ data }) => {
        const {
          updateAppUser: { appUser },
        } = data;
        setEnvironmentLocation(appUser.environmentLocation);
        setUserStep(appUser.step);
        onSuccessCallback();
      })
      .catch(() => {
        setEnvironmentLocation('');
      });
  };

  return (
    <AppUserContext.Provider
      value={{
        login,
        update,
        userId,
        entryId,
        token,
      }}
    >
      {children}
    </AppUserContext.Provider>
  );
};
