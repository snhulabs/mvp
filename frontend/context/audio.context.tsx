import * as React from 'react';

import { useLocalizedRouter } from 'hooks';
import Recorder, { SAMPLE_RATE } from 'utils/recorder';

export const AudioContext = React.createContext({
  audioContext: null,
  audioRecorder: null,
  audioBuffer: null,
  audioChunks: null,
  microphoneEnabled: false,
  permissionChecked: false,
  clearAudio: null,
  requestMicrophonePermissions: null,
  audioStream: null,
  task: null,
  setTask: null,
});

export const AudioProvider = ({ children }) => {
  const [permissionChecked, setPermissionChecked] = React.useState<boolean>(
    false
  );
  const [microphoneEnabled, setMicrophoneEnabled] = React.useState<boolean>(
    false
  );
  const [audioContext, setAudioContext] = React.useState<any>(null);
  const [audioRecorder, setAudioRecorder] = React.useState<Recorder>(null);
  const [audioBuffer, setAudioBuffer] = React.useState<ArrayBuffer>(null);
  const [audioChunks, setAudioChunks] = React.useState<[]>(null);
  const [audioStream, setAudioStream] = React.useState<MediaStream>(null);
  const [task, setTask] = React.useState(null);

  const { path } = useLocalizedRouter();

  const onRecordUpdate = () => {};

  const onRecordFinish = (outputBuffer, chunks) => {
    setAudioBuffer(outputBuffer);
    setAudioChunks(chunks);
  };

  const clearAudio = () => {
    setAudioBuffer(null);
    setAudioChunks(null);
  };

  const requestMicrophonePermissions = () => {
    navigator.mediaDevices
      .getUserMedia({
        audio: {
          advanced: [{ channelCount: 1, sampleRate: SAMPLE_RATE }],
        },
        video: false,
      })
      .then(stream => {
        //const recorder = new Recorder(stream, onRecordUpdate, onRecordFinish);
        setAudioStream(stream);
        setPermissionChecked(true);
        setMicrophoneEnabled(true);
        // setAudioRecorder(recorder);
      })
      .catch(() => {
        setAudioStream(null);
        setPermissionChecked(true);
        setMicrophoneEnabled(false);
      });
  };

  React.useEffect(() => {
    if (
      /setup|micone|mictwo|micdone|interview/.test(path) &&
      !permissionChecked
    ) {
      //TODO add the extra tasks naming
      requestMicrophonePermissions();
    }
  }, [path, permissionChecked, microphoneEnabled]);

  return (
    <AudioContext.Provider
      value={{
        audioBuffer,
        audioChunks,
        audioRecorder,
        microphoneEnabled,
        permissionChecked,
        clearAudio,
        requestMicrophonePermissions,
        audioStream,
        task,
        setTask,
        audioContext
      }}
    >
      {children}
    </AudioContext.Provider>
  );
};
