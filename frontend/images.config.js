// Automatic image optimisation for Next.js
// https://github.com/cyrilwanner/next-optimized-images/tree/canary

module.exports = {
  default: {
    webp: true,
    breakpoints: [420, 1025, 1920],
    densities: [1, 2],
  },
  types: {
    thumbnail: {
      sizes: [50, 100, 200],
    },
  },
};
