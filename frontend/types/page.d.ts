import { ParsedUrlQuery } from 'querystring';

export declare type NextEnv = {
  [key: string]: string;
};

export declare type NextContext = {
  params?: ParsedUrlQuery;
  preview?: boolean;
  previewData?: any;
  env: NextEnv;
};
