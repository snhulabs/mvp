export const MAX_ATTEMPTS = 3;
export const MIC_ONE_LENGTH = 10000;
export const MIC_TWO_LENGTH = 20000;
export const LIKEABILITY_LENGTH = 60000;
export const COMPETENCE_LENGTH = 60000;
export const PUT_ALL_TOGETHER_LENGTH = 120000;

export const DEEP_LEVEL = {
  1: 1,
  2: 10,
  3: 100,
  4: 1000,
  5: 10000,
};
