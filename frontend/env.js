const exec = require('child_process').execSync;

let ciEnv;
try {
  ciEnv = require('./.env.json');
} catch (e) {
  ciEnv = {};
}
Object.keys(ciEnv).forEach(key => {
  try {
    ciEnv[key] = JSON.parse(ciEnv[key]);
  } catch (e) {}
});

/**
 * Begin user config.
 */
const DEFAULT_ENV = {
  ENV: ciEnv.NODE_ENV || process.env.NODE_ENV,
  DEFAULT_LOCALE: 'en-us',
  USE_BROWSER_LOCALE: true,
  USE_GEOIP_LOCALE: true, // geo-ip will be more important
  GEOIP_MAPPING: {
    us: 'en-us',
    pl: 'pl',
  },
  /**
   * Removes URL prefix and forces DEFAULT_LOCALE. Useful for single-locale projects.
   */
  DISABLE_I18N: false,
  BASIC_AUTH_USERNAME: '',
  BASIC_AUTH_PASSWORD: '',
  FRONTEND_HOST: 'http://localhost:3000',
  CMS_GRAPHQL_URL: process.env.CMS_GRAPHQL_URL,
  CMS_GRAPHQL_URL_LOCAL: 'http://localhost:1337/graphql',
  VERSION: process.env.VERSION,
  BUILD_NUMBER:
    ciEnv.BUILD_NUMBER || exec('git rev-list --all --count').toString().trim(),
  BUILD_TS: new Date(),
};

const env = {
  // Default / local values
  ...DEFAULT_ENV,

  // Override with process.env
  ...ciEnv,
};

Object.keys(env).forEach(key => (process.env[key] = env[key]));

module.exports = env;
