import { MAX_ATTEMPTS } from 'config';
import { AppUserContext } from 'context/appUser.context';
import { NavigationContext, ROUTING_STEPS } from 'context/navigation.context';
import useMicrophoneRecording from 'hooks/useMicrophoneRecording';
import * as React from 'react';
import { createTask, updateTask } from 'utils/api/recording';

const useTask = (taskName: string) => {
  const [task, setTask] = React.useState(null);
  const [attempt, setAttempt] = React.useState<number>(1);
  const [showSuccess, setShowSuccess] = React.useState<boolean>(false);
  const { setupView, currentStep } = React.useContext(NavigationContext);
  const { update, token, entryId } = React.useContext(AppUserContext);
  const {
    audioContext,
    inputSource,
    audioURL,
    audioSrc,
    isProcessing,
    isRecording,
    startRecording,
    stopRecording,
    transcription,
    confidence,
    words,
    smileData,
    clearRecording,
    upload,
  } = useMicrophoneRecording();

  const onSubmit = (modifiedTranscription: string) => {
    update({ step: currentStep + 1 }, () => {
      updateTask({
        audioFile: audioURL,
        entryId: task.id,
        text: transcription,
        modifyText: modifiedTranscription,
        confidence: confidence,
        words: words,
        smileData: smileData,
        token,
      }).then(() => {
        setShowSuccess(true);
      });
    });
  };

  const onRetry = () => {
    const newAttempt = attempt + 1;
    setTask(null);
    setShowSuccess(false);
    setAttempt(newAttempt);
    clearRecording();

    createTask({
      attempt: newAttempt,
      title: taskName,
      entryId,
      token,
    }).then(task => {
      setTask(task);
    });
  };

  const canTryAgain = () => !!(MAX_ATTEMPTS - (task?.attempt || 1));

  React.useEffect(() => {
    setupView(ROUTING_STEPS.COMPETENCE);

    createTask({
      attempt,
      title: taskName,
      entryId,
      token,
    }).then(task => {
      setTask(task);
    });
  }, []);

  return {
    audioContext,
    inputSource,
    attempt,
    audioSrc,
    isProcessing,
    isRecording,
    onRetry,
    onSubmit,
    task,
    transcription,
    confidence,
    showSuccess,
    startRecording,
    stopRecording,
    canTryAgain,
    upload,
    words,
    smileData,
  };
};

export default useTask;
