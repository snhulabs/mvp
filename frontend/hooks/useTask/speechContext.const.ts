const SPEECH_CONTEXT = {
  LIKEABILITY:
    'An elderly family member is very sick but isn’t taking good care of themselves, so you call them on the phone to check in. Can you offer some support?’',
  COMPETENCE: `You are interviewing for a job. Your interviewer wants to know about your strengths as an employee.
  Can you make a 1 minute recording to describe one or two workplace skills you excel at and why these skills would contribute to the team’s success?`,
  ALL_TOGETHER:
    'Take 2 minutes to describe what you did while trying to sound as likable and competent as possible.Have you ever faced a challenge at work?',
};

export default SPEECH_CONTEXT;
