import { useEffect, useState } from 'react';
import Recorder from 'utils/recorder';

export default function useAudioRecorder(
  stream,
  onUpdate = event => {},
  onFinished = (buffer, chunks) => {}
) {
  const [recorder, setRecorder] = useState(null);

  useEffect(() => {
    const recorder = new Recorder(stream, onUpdate, onFinished);

    recorder.init().then(() => {
      setRecorder(recorder);
    });

    return () => {
      recorder.destroy();
    };
  }, []);

  return { recorder };
}
