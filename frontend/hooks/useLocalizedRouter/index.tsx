import React from 'react';
import { useRouter } from 'next/router';
import { isRemotePath, join, trimTrailingSlash } from 'utils/path';

export default function useLocalizedRouter() {
  const router = useRouter();

  const push = (href, as = null) => {
    const normalizedHref = {
      pathname: trimTrailingSlash(
        typeof href === 'string' ? href : href.pathname
      ),
      query: typeof href === 'string' || !href.query ? {} : href.query,
      hash: typeof href === 'string' || !href.hash ? '' : href.hash,
    };

    if (typeof normalizedHref.query === 'string') {
      throw new Error(
        'query values of string type are not supported. Use object notation instead.'
      );
    }

    const normalizedAs = trimTrailingSlash(
      as ? as.toString() : normalizedHref.pathname
    );

    const localizedHref = {
      pathname: normalizedHref.pathname,
      query: { ...normalizedHref.query },
      hash: normalizedHref.hash,
    };

    let localizedAs = normalizedAs;

    if (!isRemotePath(href.toString()) && !process.env.DISABLE_I18N) {
      const lang = router.query.lang;

      if (!lang) {
        throw new Error('lang not specified in router query');
      }

      localizedHref.query.lang = lang;

      // Localize internal paths
      const pathnameParts = ['/[lang]'];
      const asParts = [`/${lang}`];

      if (
        !(typeof href === 'string' ? href : href.pathname)
          .toString()
          .startsWith('/')
      ) {
        // Relative path, so prefix with current pathname
        pathnameParts.push(router.pathname);
        asParts.push(router.pathname);
      }

      // Push actual href now
      pathnameParts.push(normalizedHref.pathname);
      asParts.push(normalizedAs);

      // Override values with localized ones
      localizedHref.pathname = join(pathnameParts);
      localizedAs = join(asParts);
    }

    localizedAs += localizedHref.hash;

    router.push(localizedHref, localizedAs);
  };

  return {
    path: router.asPath,
    push,
  };
}
