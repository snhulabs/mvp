import * as React from 'react';

import Recorder from 'lib/recorder';
import { sendAudio } from 'utils/api/recording';

export type RecordingAPI = {
  clearRecording(): void;
  startRecording(): void;
  stopRecording(): void;
  upload(duration: number, speechContext?: string): Promise<void>;
  audioSrc?: string;
  isProcessing: boolean;
  isRecording: boolean;
  uploadFinished?: boolean;
  transcription?: string;
  confidence?: any;
  words?: any;
  audioURL?: string;
  smileData?: any;
  audioContext?: any;
  inputSource?: any;
};

let initialized = false;
let recorderLib = null;
let audioInput = null;
let audioStream = null;
let fileBlob = null;

const useMicrophoneRecording = (): RecordingAPI => {
  const [uploadFinished, setUploadFinished] = React.useState<boolean>(false);
  const [audioSrc, setAudioSrc] = React.useState<string>('');
  const [transcription, setTranscription] = React.useState<string>('');
  const [confidence, setConfidence] = React.useState<any>(null);
  const [words, setWords] = React.useState<any>(null);
  const [smileData, setSmileData] = React.useState<any>(null);
  const [audioURL, setAudioURL] = React.useState<string>('');
  const [isRecording, setIsRecording] = React.useState<boolean>(false);
  const [isProcessing, setIsProcessing] = React.useState<boolean>(false);
  const [audioContext, setAudioContext] = React.useState<any>(null);
  const [inputSource, setInputSource] = React.useState<any>(null);

  const mediaConfig = {
    audio: true,
    video: false,
  };

  const upload = async (duration, speechContext) => {
    setIsProcessing(true);
    const response: any = await sendAudio(fileBlob, duration, speechContext);

    setIsProcessing(false);
    if (response.data) {
      setTranscription(
        response.data ? response.data.transcription : transcription
      );
      setConfidence(response.data ? response.data.confidence : confidence);
      setAudioURL(response.data ? response.data.fileURI : audioURL);
      setWords(response.data ? new Array(response.data.words) : words);
      setSmileData(response.data ? response.data.smileData : smileData);
    }
    setUploadFinished(true);

    fileBlob = null;
  };

  const exportAudio = async blob => {
    setAudioSrc(URL.createObjectURL(blob));

    fileBlob = blob;
  };

  const startRecording = () => {
    const AudioContext =
      window.AudioContext || (window as any).webkitAudioContext;

    setAudioContext(new AudioContext())

    setTranscription('');
    setConfidence(null);
  };

  React.useEffect(() => {
    if (audioContext == null) return;

    navigator.mediaDevices.getUserMedia(mediaConfig).then(stream => {
      audioStream = stream;
      audioInput = audioContext.createMediaStreamSource(stream);

      setInputSource(audioInput)
    });
  }, [audioContext])

  React.useEffect(() => {
    if (inputSource == null) return;

    recorderLib = new Recorder(inputSource, {
      numChannels: 1,
    });
    recorderLib.record();
    setIsRecording(true);

  }, [inputSource]);

  const stopRecording = () => {
    recorderLib.stop();
    audioStream.getAudioTracks()[0].stop();
    recorderLib.exportWAV(exportAudio);

    setIsRecording(false);
  };

  const clearRecording = () => {
    if (recorderLib) {
      recorderLib.stop();
    }

    if (audioContext) {
      audioContext.close();
    }

    initialized = false;
    setAudioContext(null);
    recorderLib = null;
    audioInput = null;
    audioStream = null;
    fileBlob = null;

    setUploadFinished(false);
    setAudioSrc('');
    setTranscription('');
    setIsRecording(false);
    setIsProcessing(false);
  };

  React.useEffect(() => {
    initialized = true;
  }, []);

  React.useEffect(() => {
    setUploadFinished(false);

    return () => {
      if (recorderLib) {
        recorderLib.stop();
      }

      if (audioContext) {
        audioContext.close();
      }

      initialized = false;
      setAudioContext(null);
      recorderLib = null;
      audioInput = null;
      audioStream = null;
      fileBlob = null;
    };
  }, []);

  return {
    audioURL,
    clearRecording,
    uploadFinished,
    audioSrc,
    isProcessing,
    isRecording,
    startRecording,
    stopRecording,
    upload,
    transcription,
    confidence,
    words,
    smileData,
    audioContext,
    inputSource
  };
};

export default useMicrophoneRecording;
