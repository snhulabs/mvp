import * as React from 'react';

const useTimeout = ({
  action,
  beforeAction,
  timeout,
  condition = true,
  dependencies = [],
}) => {
  const timeoutHandler = React.useRef(null);

  const internalExecute = () => {
    if (condition) {
      !!beforeAction && beforeAction();

      timeoutHandler.current = setTimeout(() => {
        action();

        clearTimeout(timeoutHandler.current);
        timeoutHandler.current = null;
      }, timeout);
    }
  }

  React.useEffect(() => {
    internalExecute();

    return () => {
      clearTimeout(timeoutHandler.current);
      timeoutHandler.current = null;
    };
  }, dependencies);

  return {
    execute: () => {
      internalExecute();
    },
    clear: () => {
      clearTimeout(timeoutHandler.current);
      timeoutHandler.current = null;
    },
  };
};

export default useTimeout;
